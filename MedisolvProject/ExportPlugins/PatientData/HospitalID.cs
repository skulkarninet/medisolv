﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using VSMMefMvc.Interfaces;

namespace ExportPlugins.PatientData
{
    [Export(typeof(IHospitalID))]
    public class HospitalID:IHospitalID
    {
        /// <summary>
        /// Gets the hospital identifier.
        /// </summary>
        /// <param name="measureSet">The measure set.</param>
        /// <returns>List&lt;System.String&gt;.</returns>
        public List<string> getHospitalID()
        {
            HBIPSEVTHospID hbipsevtHospId = new HBIPSEVTHospID();

            List<string> lstHospitalID = new List<string>();            

            using (RapidEntities context = new RapidEntities())
            {
                //Creation of List of Patients specific to Provider

                //Access database tables             
                DbSet<MeasureSet> measureSets = context.MeasureSets;
                DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                var hospitalIDList = from  lstHospitals in hospitalDefaults                                            
                                     select new
                    {
                        lstHospitals.HospitalID
                    };
                var distinctHospitalIDList = hospitalIDList.Distinct();
                foreach (var hospitalID in distinctHospitalIDList)
                {
                    lstHospitalID.Add(hospitalID.HospitalID.ToString());
                }
               
            }
            
            return lstHospitalID;
        }
    }
}
