﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;

namespace ExportPlugins.PatientData
{
    public class HBIPSEVTHospID
    {
        public List<string> getHospitalID(string measureSet, DateTime startDate, DateTime endDate, string selectedSpeciality)
        {
            List<string> lstHospitalID = new List<string>();
            measureSet = "HBIPS-EVT";
            using (RapidEntities context = new RapidEntities())
            {
                //Creation of List of Patients specific to Provider

                //Access database tables             
                DbSet<MeasureSet> measureSets = context.MeasureSets;
                DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                DbSet<EventPatientPopulation> eventPatientPopulations = context.EventPatientPopulations;
                DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                var hospitalIDList = from lstMeasures in measureSets
                    join lstVersions in versions
                        on lstMeasures.VersionKey equals lstVersions.VersionKey
                    join lstIppsnaps in eventPatientPopulations
                        on lstMeasures.MeasureSetKey equals lstIppsnaps.MeasureSetKey
                    join lstHospitals in hospitalDefaults
                        on lstIppsnaps.HospitalID equals lstHospitals.HospitalID
                    where
                        lstIppsnaps.MeasureSetKey == lstMeasures.MeasureSetKey &&
                        startDate <= lstVersions.EndDate &&
                        lstVersions.StartDate <= endDate &&
                        lstMeasures.MeasureSetID == measureSet    
                    select new
                    {
                        lstHospitals.HospitalID
                    };
                var distinctHospitalIDList = hospitalIDList.Distinct();
                foreach (var hospitalID in distinctHospitalIDList)
                {
                    lstHospitalID.Add(hospitalID.HospitalID.ToString());
                }

            }
            return lstHospitalID;
        }
    }
}
