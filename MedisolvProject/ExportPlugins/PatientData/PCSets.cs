﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    /// Class PCSets.
    /// </summary>
    public class PCSets
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="PCSets"/> class.
        /// </summary>     
        public PCSets()
        {
            _log = Container.ResolveLoggerInstance();
        }

        public ExportParameters getPCM(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getPCM");

                List<PatientModel> lstPatient = new List<PatientModel>();                

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "PC" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "CLNCLTRIAL" || lstCDatas.DataKey == "ACTLABOR" ||
                            //  lstCDatas.DataKey == "GESTAGE" ||
                            //  lstCDatas.DataKey == "PARITY" || lstCDatas.DataKey == "ANTSTADM" ||
                            //  lstCDatas.DataKey == "RSNNOANTST" ||
                            //  lstCDatas.DataKey == "PRUTRNSURG" || lstCDatas.DataKey == "HOSPADMIN" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_2") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.IPPIdentity,
                            lstIppsnaps.HospitalID
                        };
                    var ptlistPCM = patientdatalist.Distinct();
                    foreach (var patient in ptlistPCM)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "PCM";
                        patientModel.Export = "PCM";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getPCM ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getPCM " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the PCBF.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getPCBF(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getPCBF");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "PC" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "TERMNB" || lstCDatas.DataKey == "CLNCLTRIAL" ||
                            //  lstCDatas.DataKey == "EXCBRTFED"
                            //  || lstCDatas.DataKey == "ADMSNNICU" || lstCDatas.DataKey == "HOSPADMIN" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1"
                            //  || lstCDatas.DataKey == "PHYSICIAN_2") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistPCBF = patientdatalist.Distinct();
                    foreach (var patient in ptlistPCBF)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "PC-BF";
                        patientModel.Export = "PC_BF";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getPCBF ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getPCBF " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the pcbsi.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getPCBSI(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getPCBSI");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
            {
                //Creation of List of Patients specific to Provider

                //Access database tables
                DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                DbSet<CaseData> casedatas = context.CaseDatas;
                DbSet<MeasureSet> measureSets = context.MeasureSets;
                DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                var patientdatalist = from lstPatients in initialPatientPopulations
                                      join lstMeasures in measureSets
                                          on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                      join lstVersions in versions
                                          on lstMeasures.VersionKey equals lstVersions.VersionKey
                                      join lstIppsnaps in ippSnapshots
                                          on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                      join lstProviders in hospitalDefaults
                                          on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                      //join lstCDatas in casedatas
                                      //on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                                      orderby lstIppsnaps.AccountNumber
                                      where
                                          startDate <= lstVersions.EndDate &&
                                          lstVersions.StartDate <= endDate &&
                                          lstMeasures.MeasureSetID == "PC" &&
                                          lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                                          lstProviders.HospitalDefaultName == "ProviderID" &&
                                          lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                                         // lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                                         //((lstCDatas.DataKey == "BIRTHWEIGHT" || lstCDatas.DataKey == "BLDINFCFMD" || lstCDatas.DataKey == "BLDINFPRSADM"
                                         //|| lstCDatas.DataKey == "CLNCLTRIAL" || lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "PHYSICIAN_1" 
                                         //|| lstCDatas.DataKey == "PHYSICIAN_2") &&(
                                         //!lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                                      select new
                                      {
                                          lstIppsnaps.UnitNumber,
                                          lstIppsnaps.PatientName,
                                          lstIppsnaps.AccountNumber,
                                          lstMeasures.MeasureSetID,
                                          lstIppsnaps.HospitalID,
                                          lstIppsnaps.IPPIdentity
                                      };
                var ptlistPCBSI = patientdatalist.Distinct();
                foreach (var patient in ptlistPCBSI)
                {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "PC-BSI";
                        patientModel.Export = "PC_BSI";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
            }
                _log.Info("Exit for Export method of getPCBSI ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getPCBSI " + ex.Message);
                return null;
            }
        }
    }
}
