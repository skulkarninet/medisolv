﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using EncorExportWebUI.Models;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    /// This class gets name of Global MeasureSet and Returns the Patient details 'Account Number','Name','MeasureSet','Unit Number' for Grid
    /// </summary>
    public class GlobalSets
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSExport"/> class.
        /// </summary>     
      //  public bool Flag;
        public GlobalSets()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// Gets the ED.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getEd(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getEd");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "GLOBAL" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                            lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "ARRVLDATE" || lstCDatas.DataKey == "ARRVLTIME" ||
                            //  lstCDatas.DataKey == "DCNADMITDT" || lstCDatas.DataKey == "DCNADMITTM" ||
                            //  lstCDatas.DataKey == "EDDEPARTDT" || lstCDatas.DataKey == "EDDEPARTTM" ||
                            //  lstCDatas.DataKey == "EDPATIENT" || lstCDatas.DataKey == "HOSPADMIN" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1" || lstCDatas.DataKey == "PHYSICIAN_2") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistED = patientdatalist.Distinct();
                    int count = ptlistED.Count();
                    foreach (var patient in ptlistED)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "ED";
                        patientModel.Export = "ED";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }

                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getEd " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the imm.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getIMM(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getIMM");

                List<PatientModel> lstPatient = new List<PatientModel>();            

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "GLOBAL" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                            lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "PNEVACSTATUS" || lstCDatas.DataKey == "FLUVACSTATUS" ||
                            //  lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "PHYSICIAN_1" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_2") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistIMM = patientdatalist.Distinct();
                    foreach (var patient in ptlistIMM)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "IMM";
                        patientModel.Export = "IMM";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getIMM " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the sub.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getSUB(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getSUB");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
            {
                //Creation of List of Patients specific to Provider

                //Access database tables
                DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                DbSet<CaseData> casedatas = context.CaseDatas;
                DbSet<MeasureSet> measureSets = context.MeasureSets;
                DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                var patientdatalist = from lstPatients in initialPatientPopulations
                    join lstMeasures in measureSets
                        on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                    join lstVersions in versions
                        on lstMeasures.VersionKey equals lstVersions.VersionKey
                    join lstIppsnaps in ippSnapshots
                        on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                    join lstProviders in hospitalDefaults
                        on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                    //join lstCDatas in casedatas
                    //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                    orderby lstIppsnaps.AccountNumber
                    where
                        startDate <= lstVersions.EndDate &&
                        lstVersions.StartDate <= endDate &&
                        lstMeasures.MeasureSetID == "GLOBAL" &&
                        lstPatients.VisitStatus != "X" &&
                        lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                        lstProviders.HospitalDefaultName == "ProviderID" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                        //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                        //((lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "ALCDRGDISORD" ||
                        //  lstCDatas.DataKey == "ALCSTATUS"
                        //  || lstCDatas.DataKey == "BRFINTVTN" || lstCDatas.DataKey == "FUCONTACT" ||
                        //  lstCDatas.DataKey == "FUCONTACTDT"
                        //  || lstCDatas.DataKey == "REFADDTX" || lstCDatas.DataKey == "RXALCDRGMED" ||
                        //  lstCDatas.DataKey == "ALCODRUGPOSTDISCH"
                        //  || lstCDatas.DataKey == "ALCSTATDCCOUN" || lstCDatas.DataKey == "ALCSTATDCMED" ||
                        //  lstCDatas.DataKey == "ALCSTATDCQUIT"
                        //  || lstCDatas.DataKey == "DRGSTATDCQUIT" || lstCDatas.DataKey == "PHYSICIAN_1" ||
                        //  lstCDatas.DataKey == "PHYSICIAN_2" || lstCDatas.DataKey == "COMFORTMX") && (
                        //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                    select new
                    {
                        lstIppsnaps.UnitNumber,
                        lstIppsnaps.PatientName,
                        lstIppsnaps.AccountNumber,
                        lstMeasures.MeasureSetID,
                        lstIppsnaps.HospitalID,
                        lstIppsnaps.IPPIdentity
                    };
                var ptlistSUB = patientdatalist.Distinct();
                foreach (var patient in ptlistSUB)
                {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "SUB";
                        patientModel.Export = "SUB";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
            }
                return new ExportParameters() { PatientList = lstPatient };
            }
         catch (Exception ex)
        {
                _log.Error("Error occured while exporting getSUB " + ex.Message);
                return null;
        }
}

        /// <summary>
        /// Gets the tob.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getTOB(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getTOB");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "GLOBAL" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                            lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "REFOPTOBCSNG" ||
                            //  lstCDatas.DataKey == "TOBSTATUS"
                            //  || lstCDatas.DataKey == "RXTOBMED" || lstCDatas.DataKey == "FUCONTACT" ||
                            //  lstCDatas.DataKey == "FUCONTACTDT"
                            //  || lstCDatas.DataKey == "TOBTXCOUNS" || lstCDatas.DataKey == "TOBTXMED" ||
                            //  lstCDatas.DataKey == "TOBUSEPOSTDISCH"
                            //  || lstCDatas.DataKey == "RSNNOTOBDC" || lstCDatas.DataKey == "RSNNOTOBSTAY" ||
                            //  lstCDatas.DataKey == "TOBSTATDCCOUN"
                            //  || lstCDatas.DataKey == "TOBSTATDCMED" || lstCDatas.DataKey == "TOBSTATDCQUIT" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1"
                            //  || lstCDatas.DataKey == "PHYSICIAN_2" || lstCDatas.DataKey == "COMFORTMX") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistTOB = patientdatalist.Distinct();
                    foreach (var patient in ptlistTOB)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "TOB";
                        patientModel.Export = "TOB";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getTOB" + ex.Message);
                return null;
            }
        }
    }
}
