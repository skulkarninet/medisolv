﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using VSMMefMvc.Interfaces;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'ACUTE MYOCARDIAL INFARCTION (AMI)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IPatientDataService))]

    public class PatientInfo : IPatientDataService
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSExport"/> class.
        /// </summary>     
        public PatientInfo()
        {
            _log = Container.ResolveLoggerInstance();
        }

        public Enum measEnum;
        /// <summary>
        /// Gets the patient information.
        /// </summary>
        /// <param name="measure">The measure.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters GetPatientInfo(List<string> measure, DateTime startDate, DateTime endDate,string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for GetPatientInfo");
                List<string> meas = new List<string>();
                List<PatientModel> lstPatient = new List<PatientModel>();
                ExportParameters param=new ExportParameters();
                ExportParameters parameters=new ExportParameters();
                Hashtable patients=new Hashtable();               
                GlobalSets global=new GlobalSets();
                PCSets pc = new PCSets();
                IPFSets ipf = new IPFSets();
                HBIPSEVT hbips = new HBIPSEVT();
                NameOPPS name = new NameOPPS();
                HIC hic = new HIC();
                IPPSOPPSSets allCommon=new IPPSOPPSSets();
                Hashtable checkMeasure = new Hashtable();
                parameters.PatientList = new List<PatientModel>();
                meas.AddRange(measure);
                if (measure.Contains("ANTIBIOTIC"))
                    measure[measure.IndexOf("ANTIBIOTIC")] = "SCIP";
                if (measure.Contains("HBIPS"))
                    measure[measure.IndexOf("HBIPS")] = "HBIPS-DSC";
                               
                if (selectedSpeciality == "OPPS")
                {
                    if (measure.Contains("AMI"))
                        measure[measure.IndexOf("AMI")] = Constants.OPAMI;
                    if (measure.Contains("CATRCTO"))
                        measure[measure.IndexOf("CATRCTO")] = Constants.OPCataracts;
                    if (measure.Contains("CP"))
                        measure[measure.IndexOf("CP")] = Constants.OPCHESTPAIN;
                    if (measure.Contains("ED"))
                        measure[measure.IndexOf("ED")] = Constants.OPED;
                    if (measure.Contains("ENDFCO"))
                        measure[measure.IndexOf("ENDFCO")] = Constants.OPENDFC_O;
                    if (measure.Contains("ENDPCO"))
                        measure[measure.IndexOf("ENDPCO")] = Constants.OPENDPC_O;
                    if (measure.Contains("STK"))
                        measure[measure.IndexOf("STK")] = Constants.OPSTK;
                    if (measure.Contains("PM"))
                        measure[measure.IndexOf("PM")] = Constants.OPPAIN;                    
                }
                //foreach (var measureSet in measure)
                for(int i=0;i<measure.Count;i++)
                {
                    switch (measure[i])
                    {
                        case "ED":
                            param = global.getEd(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "IMM":
                            param = global.getIMM(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "SUB":
                            param = global.getSUB(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "TOB":
                            param = global.getTOB(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "PCM":
                            param = pc.getPCM(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "PC_BF":
                            param = pc.getPCBF(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "PC_BSI":
                            param = pc.getPCBSI(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "IPF_IMM":
                            param = ipf.getIPFIMM(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "IPF_SUB":
                            param = ipf.getIPFSUB(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "IPF_TOB":
                            param = ipf.getIPFTOB(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "HBIPS_EVT":
                            param = hbips.getHBIPSEVT(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);
                            break;
                        case "HIC":
                            param = hic.getHIC(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList); 
                            break;
                        //case "COMPLETEDATA":
                        //    param = allCommon.getCompleteData(measure[i], startDate, endDate, selectedSpeciality, selectedHospitalID);
                        //    parameters.PatientList.AddRange(param.PatientList);
                        //    break;
                        case "NAME":
                            param = name.getNAME(startDate, endDate, selectedSpeciality, selectedHospitalID);
                            parameters.PatientList.AddRange(param.PatientList);                     
                            break;
                        default:
                            param = allCommon.getAllMeasureSets(measure[i], startDate, endDate, selectedSpeciality,selectedHospitalID, meas[i]);
                            parameters.PatientList.AddRange(param.PatientList);                            
                            break;
                    }

                }

                _log.Info("Exit for Export method of GetPatientInfo ");
                return new ExportParameters() { PatientList = parameters.PatientList };
            }        
        catch (Exception ex)
        {
                _log.Error("Error occured while exporting GetPatientInfo " + ex.Message);
                return null;
        }

        }       

    }
}
