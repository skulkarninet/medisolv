﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    ///  This class gets name of IPF and Returns the Patient details 'Account Number','Name','MeasureSet','Unit Number' for Grid
    /// </summary>
    public class IPFSets
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPFSetsExport"/> class.
        /// </summary>     
        public IPFSets()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// Gets the ipfimm.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getIPFIMM(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getIPFIMM");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "HBIPS-GLOBAL" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "FLUVACSTATUS" || lstCDatas.DataKey == "HOSPADMIN" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1"
                            //  || lstCDatas.DataKey == "PHYSICIAN_2") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistIPFIMM = patientdatalist.Distinct();
                    foreach (var patient in ptlistIPFIMM)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "IPF-IMM";
                        patientModel.Export = "IPF_IMM";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getIPFIMM ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getIPFIMM " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the ipfsub.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getIPFSUB(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getIPFSUB");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "HBIPS-GLOBAL" &&
                             lstPatients.VisitStatus != "X" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "ALCSTATUS" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1"
                            //  || lstCDatas.DataKey == "PHYSICIAN_2" || lstCDatas.DataKey == "COMFORTMX") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistIPFSUB = patientdatalist.Distinct();
                    foreach (var patient in ptlistIPFSUB)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "IPF-SUB";
                        patientModel.Export = "IPF_SUB";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getIPFSUB ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getIPFSUB " + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the ipftob.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getIPFTOB(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getIPFTOB");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        //join lstCDatas in casedatas
                        //    on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "HBIPS-GLOBAL" &&
                            lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                            //lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                            //((lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "TOBSTATUS" ||
                            //  lstCDatas.DataKey == "TOBTXCOUNS"
                            //  || lstCDatas.DataKey == "TOBTXMED" || lstCDatas.DataKey == "RSNNOTOBSTAY" ||
                            //  lstCDatas.DataKey == "PHYSICIAN_1"
                            //  || lstCDatas.DataKey == "PHYSICIAN_2" || lstCDatas.DataKey == "COMFORTMX") && (
                            //      !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)))
                        select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.HospitalID,
                            lstIppsnaps.IPPIdentity
                        };
                    var ptlistIPFTOB = patientdatalist.Distinct();
                    foreach (var patient in ptlistIPFTOB)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = "IPF-TOB";
                        patientModel.Export = "IPF_TOB";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getIPFTOB ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getIPFTOB " + ex.Message);
                return null;
            }
        }
    }
}
