﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    ///  This class gets name of MeasureSets other than Global for IPPS and OPPS and Returns the Patient details 'Account Number','Name','MeasureSet','Unit Number' for Grid
    /// </summary>
    public class IPPSOPPSSets
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSOPPSSets"/> class.
        /// </summary>     
        public IPPSOPPSSets()
        {
            _log = Container.ResolveLoggerInstance();
        }

        public bool checkUnknownMeasureSet;
        public bool flag2;

        public ExportParameters getAllMeasureSets(string measure, DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID,string mea)
        {
            try
            {
                _log.Info("Entered Export XML for getAllMeasureSets");              

                List<PatientModel> lstPatient = new List<PatientModel>();

                //_log.Info("Entered DataFetch Method of DataMapper");
                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();
                Provider provider = new Provider();
                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider
                    provider.Patient = new List<Patient>();

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                       orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            ((lstMeasures.MeasureSetID == measure &&
                            (measure != "COMPLETEDATA" && measure != "DEMOGRAPHIC" && measure != "NAME" &&
                             measure != "HIC" && measure != "GLOBAL" && measure != "PC" && measure != "HBIPS-GLOBAL")) ||
                            (measure == "COMPLETEDATA" || measure == "DEMOGRAPHIC" || measure == "NAME")) &&
                            ((lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                             selectedSpeciality == "IPPS") ||
                            (lstIppsnaps.InpatientOrOutpatient == Constants.Specification.O.ToString() &&
                            selectedSpeciality == "OPPS")) &&
                             lstProviders.HospitalDefaultName == "ProviderID" &&
                            lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID==null))
                                          select new 
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.IPPIdentity,
                            lstIppsnaps.HospitalID
                           
                        };

                    var ptlist = patientdatalist.Distinct();
                
                    foreach (var patient in ptlist)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = patient.MeasureSetID;
                        patientModel.Export = mea;
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
          
                    }
                }
                               
                _log.Info("Exit for Export method of getAllMeasureSets ");
                return new ExportParameters() {PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getAllMeasureSets " + ex.Message);
                return null;
            }
        }

        public ExportParameters getCompleteData(string measure, DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {         
            try
            {
                _log.Info("Entered Export XML for getCompleteData");

                List<PatientModel> lstPatient = new List<PatientModel>();

                //_log.Info("Entered DataFetch Method of DataMapper");
                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();
                Provider provider = new Provider();
                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider
                    provider.Patient = new List<Patient>();

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)
                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            ((lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                              selectedSpeciality == "IPPS") ||
                             (lstIppsnaps.InpatientOrOutpatient == Constants.Specification.O.ToString() &&
                              selectedSpeciality == "OPPS")) &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null))
                                          select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.IPPIdentity
                         };

                    var ptlist = patientdatalist;
                    DataMapper.GlobalSets ed = new DataMapper.GlobalSets();
                    int count = ptlist.Count();
                    foreach (var patient in ptlist)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = patient.MeasureSetID;
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
               
                    _log.Info("Exit for Export method of getCompleteData ");
                    return new ExportParameters() { PatientList = lstPatient };
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getCompleteData " + ex.Message);
                return null;
            }
        }
    }
}
