﻿using System;
using System.ComponentModel.Composition;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using ExportPlugins.DataAccess;
using EncorExportWebUI.ViewModels;
using System.Xml.Serialization;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using System.Collections;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    public class NameOPPS
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSExport"/> class.
        /// </summary>     
        public NameOPPS()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// Gets the hic.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getNAME(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getNAME");

                List<PatientModel> lstPatient = new List<PatientModel>();

                Submission submission = new Submission();
                submission.Provider = new List<Provider>();
                Provider provider = new Provider();

                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider
                    provider.Patient = new List<Patient>();

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;


                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)

                    var patientdatalist = from lstPatients in initialPatientPopulations
                                          join lstMeasures in measureSets
                                              on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                          join lstVersions in versions
                                              on lstMeasures.VersionKey equals lstVersions.VersionKey
                                          join lstIppsnaps in ippSnapshots
                                              on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                          join lstProviders in hospitalDefaults
                                              on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                             join lstCDatas in casedatas
                                             on lstIppsnaps.IPPIdentity equals lstCDatas.IPPIdentity
                                          orderby lstIppsnaps.AccountNumber
                                          where
                                              startDate <= lstVersions.EndDate &&
                                              lstVersions.StartDate <= endDate &&
                                              lstIppsnaps.InpatientOrOutpatient == Constants.Specification.O.ToString() &&
                                              lstProviders.HospitalDefaultName == "ProviderID" &&
                                              lstPatients.VisitStatus != "X" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) &&
                                              (lstCDatas.DataKey== "MS_PatientFirstName" || lstCDatas.DataKey== "MS_PatientLastName")
                                          select new
                                          {
                                              IppKey = lstPatients.IPPKey,                                             
                                              UnitNo = lstIppsnaps.UnitNumber,
                                              AccNo = lstIppsnaps.AccountNumber,
                                              name=lstIppsnaps.PatientName,
                                              MeasureID = lstMeasures.MeasureSetID,  
                                              hospitalID=lstIppsnaps.HospitalID                                           
                                          };
                    var ptlisthic = patientdatalist.Distinct();
                    foreach (var patient in ptlisthic)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccNo;
                        patientModel.patientNumbers = patient.UnitNo;
                        patientModel.patientName = patient.name;
                        patientModel.MeasureSet = patient.MeasureID;
                        patientModel.Export = "NAME";
                        patientModel.HospitalID = patient.hospitalID.ToString();
                        patientModel.IPPKey = patient.IppKey.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getNAME ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getNAME" + ex.Message);
                return null;
            }
        }
    }
}


