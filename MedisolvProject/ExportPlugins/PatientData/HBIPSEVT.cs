﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.Model;
using TracerX;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    /// This class gets name of HBIPS-EVT MeasureSet and Returns the Patient details 'Account Number','Name','MeasureSet','Unit Number' for Grid
    /// </summary>
    public class HBIPSEVT
    {

        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSExport"/> class.
        /// </summary>     
        public bool Flag;
        List<bool> check = new List<bool>();
        public HBIPSEVT()
        {
            _log = Container.ResolveLoggerInstance();
        }

        /// <summary>
        /// Gets the hbipsevt.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getHBIPSEVT(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getHBIPSEVT");
              
                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables

                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    DbSet<EventPatientPopulation> eventPatientPopulations = context.EventPatientPopulations;

                    var patientdatalist = from lstVersions in versions
                        join lstMeasures in measureSets
                            on lstVersions.VersionKey equals lstMeasures.VersionKey
                        join lstPatients in eventPatientPopulations
                            on lstMeasures.MeasureSetKey equals lstPatients.MeasureSetKey
                        join lstProviders in hospitalDefaults
                            on lstPatients.HospitalID equals lstProviders.HospitalID
                        orderby lstPatients.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            lstMeasures.MeasureSetID == "HBIPS-EVT" &&
                           lstPatients.VisitStatus != "X" &&
                             lstProviders.HospitalDefaultName == "ProviderID" &&
                             ((lstProviders.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstProviders.HospitalID.ToString() != null && selectedHospitalID == null))
                                          select new
                        {
                            lstPatients.UnitNumber,
                            lstPatients.PatientName,
                            lstPatients.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstPatients.HospitalID,
                            lstPatients.EventKey
                        };
                    var ptlisthbipsevt = patientdatalist.Distinct();
                    foreach (var patient in ptlisthbipsevt)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = patient.MeasureSetID;
                        patientModel.Export = "HBIPS_EVT";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.EventKey.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getHBIPSEVT ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getHBIPSEVT " + ex.Message);
                return null;
            }
        }
    }
}
