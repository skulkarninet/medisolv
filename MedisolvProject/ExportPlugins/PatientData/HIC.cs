﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;
using VSMMefMvc.Models;

namespace ExportPlugins.PatientData
{
    /// <summary>
    /// This class gets name of HIC and Returns the Patient details 'Account Number','Name','MeasureSet','Unit Number' for Grid
    /// </summary>
    public class HIC
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSExport"/> class.
        /// </summary>     
        public HIC()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// Gets the hic.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>Hashtable.</returns>
        public ExportParameters getHIC(DateTime startDate, DateTime endDate, string selectedSpeciality, string selectedHospitalID)
        {
            try
            {
                _log.Info("Entered Export XML for getHIC");

                List<PatientModel> lstPatient = new List<PatientModel>();

                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                    DbSet<EventPatientPopulation> eventPatientPopulations = context.EventPatientPopulations;

                    var patientdatalist = from lstPatients in initialPatientPopulations
                        join lstMeasures in measureSets
                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                        join lstVersions in versions
                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                        join lstIppsnaps in ippSnapshots
                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                        join lstProviders in hospitalDefaults
                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                        join lstCDatas in casedatas
                            on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                        orderby lstIppsnaps.AccountNumber
                        where
                            startDate <= lstVersions.EndDate &&
                            lstVersions.StartDate <= endDate &&
                            ((lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                              selectedSpeciality == "IPPS") ||
                             (lstIppsnaps.InpatientOrOutpatient == Constants.Specification.O.ToString() &&
                              selectedSpeciality == "OPPS")) &&
                             lstPatients.VisitStatus != "X" &&
                            lstCDatas.DataKey == "MS_Payment" && lstCDatas.DataValue.StartsWith("Medicare~") &&
                            lstProviders.HospitalDefaultName == "ProviderID" &&
                             ((lstIppsnaps.HospitalID.ToString() == selectedHospitalID && selectedHospitalID != null) || (lstIppsnaps.HospitalID != null && selectedHospitalID == null)) 
                                          select new
                        {
                            lstIppsnaps.UnitNumber,
                            lstIppsnaps.PatientName,
                            lstIppsnaps.AccountNumber,
                            lstMeasures.MeasureSetID,
                            lstIppsnaps.IPPIdentity,
                            lstIppsnaps.HospitalID,
                            lstCDatas.DataValue
                         };
                    var ptlisthic = patientdatalist.Distinct();
                    foreach (var patient in ptlisthic)
                    {
                        var patientModel = new PatientModel();
                        patientModel.AccountNumber = patient.AccountNumber;
                        patientModel.patientNumbers = patient.UnitNumber;
                        patientModel.patientName = patient.PatientName;
                        patientModel.MeasureSet = patient.MeasureSetID;
                        patientModel.Export = "HIC";
                        patientModel.HospitalID = patient.HospitalID.ToString();
                        patientModel.IPPKey = patient.IPPIdentity.ToString();
                        patientModel.Selected = false;
                        lstPatient.Add(patientModel);
                    }
                }
                _log.Info("Exit for Export method of getHIC ");
                return new ExportParameters() { PatientList = lstPatient };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting getHIC " + ex.Message);
                return null;
            }

        }
    }
}
