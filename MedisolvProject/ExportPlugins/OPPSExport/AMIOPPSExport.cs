﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using System.Data.Entity;
using ExportPlugins.DataAccess;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;

namespace VSMMefMvc.BusinessRules
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'ACUTE MYOCARDIAL INFARCTION (AMI)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "OPPS-AMI")]
   public class AMIOPPSExport :IExportService
    {
         Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="AMIOPPSExport"/> class.
        /// </summary>
        public AMIOPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
         /// <summary>
         /// XML file storage path
         /// </summary>   
         private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

         ExportPlugins.DataMapper.OPPSDataMapper OppsdataMapping = new ExportPlugins.DataMapper.OPPSDataMapper();

         /// <summary>
         /// This IExportService interface Method generates Export of AMI MeasureSet
         /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
              try
            {
                _log.Info("Entered Export XML for AMIOPPS");
                //Object of Submission class to store result of 'DataFetch' method 
                Submission submission=new Submission();

                //Call to 'DataFetch' Method 
                submission = OppsdataMapping.OPPSDataFetch(exportParameters, Constants.OPAMI, "AMI");

                //If Patient data is not available 
                if(submission==null)
                    return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                //If Patient data is available
                else
                {
                    XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                    XmlAttributes attrs = new XmlAttributes();

                    /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                       applied to the Comment field. Thus it will be serialized.*/
                    attrs.XmlIgnore = true;
                    xOver.Add(typeof(Encounter), "pthic", attrs);

                    //attrs.XmlIgnore = false;
                    //xOver.Add(typeof(Encounter), "Detail", attrs);
                    //Call to 'GenerateXml' Method for Serialization
                    XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                    XmlFilePath ="AMIOPPS";
                    XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters);

                    _log.Info("Exit for Export method of AMI OPPS Measure");
                    return new ExportResult() { IsValid = true };     
                }                                                                                        
            }
            catch (Exception ex)
            {
                 _log.Error("Error occured while exporting AMI OPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = ex.ToString()};             
            }             
        }

        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
