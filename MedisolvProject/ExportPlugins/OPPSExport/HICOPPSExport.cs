﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.OPPSExport
{

    /// <summary>
    /// This class Generates XML file for MeasureSet 'ACUTE MYOCARDIAL INFARCTION (AMI)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "OPPS-HIC")]
    class HICOPPSExport : IExportService
    {
        Logger _log;
        public HICOPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        public bool checkMultipleMeasureSet = false;
        public bool checkUnknownMeasureSet = false;
        public bool checkPthic = false;

        DataMapper.IPPSDataMapper dataMapping = new DataMapper.IPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of HIC MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
            try
            {
                _log.Info("Entered Export XML for HICOPPS");
                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();

                List<string> lstSelectedIPPKey = new List<string>();
                List<string> lstSelectedMeasureSet = new List<string>();

                //Gets selected Patient Account number and MeasureSet from Grid
                var lstAccountNum = exportParameters.PatientList.Where(m => m.Selected == true).Select(m => m.AccountNumber).ToList();
                var lstmeasureset = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "HIC")).Select(m => m.MeasureSet).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "HIC")).Select(m => m.IPPKey).ToList();


                lstSelectedIPPKey = ippkeys;
                lstSelectedMeasureSet = lstmeasureset;

                List<KeyValuePair<string, string>> Acc_MeasureSet_Collection = new List<KeyValuePair<string, string>>();
                for (int j = 0; j < lstSelectedIPPKey.Count; j++)
                {
                    Acc_MeasureSet_Collection.Add(new KeyValuePair<string, string>(lstSelectedIPPKey[j], lstSelectedMeasureSet[j]));
                }


                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //Constant values for unavailable mapping fields of Submission Class
                    //provider.Providerid = Constants.SubmissionProviderid;
                    submission.Type = Constants.SubmissionTypeOutpatient;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)
                   
                    var patientdatalist = from Providerslst in hospitalDefaults
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID"
                                          select new
                                          {
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              patients = from lstPatients in initialPatientPopulations
                                                         join lstMeasures in measureSets
                                                             on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                                         join lstVersions in versions
                                                             on lstMeasures.VersionKey equals lstVersions.VersionKey
                                                         join lstIppsnaps in ippSnapshots
                                                             on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                                         join lstCDatas in casedatas
                                                              on lstPatients.IPPKey equals lstCDatas.IPPIdentity
                                                         join lstProviders in hospitalDefaults
                                                       on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                                         orderby lstIppsnaps.AccountNumber
                                                         where
                                                             exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                             lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                             //(lstMeasures.MeasureSetID == Constants.OPAMI || lstMeasures.MeasureSetID == Constants.OPCHESTPAIN || lstMeasures.MeasureSetID == Constants.OPED || lstMeasures.MeasureSetID == Constants.OPSTK || lstMeasures.MeasureSetID == Constants.OPPAIN) &&
                                                             (lstAccountNum.Contains(lstIppsnaps.AccountNumber) && lstmeasureset.Contains(lstMeasures.MeasureSetID) && (ippkeys.Contains(lstIppsnaps.IPPIdentity.ToString()))) &&
                                                             lstIppsnaps.InpatientOrOutpatient == Constants.Specification.O.ToString() &&
                                                              lstProviders.HospitalDefaultName == "ProviderID" &&
                                                              lstIppsnaps.HospitalID == Providerslst.HospitalID &&
                                                             ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null)) &&
                                                             lstCDatas.DataKey == "MS_Payment" && lstCDatas.DataValue.StartsWith("Medicare~")
                                                         select new
                                                         {
                                                             IppKey = lstPatients.IPPKey,
                                                             Adate = lstIppsnaps.AdmitDateTime,
                                                             Ddate = lstIppsnaps.DischargeDateTime,
                                                             UnitNo = lstIppsnaps.UnitNumber,
                                                             AccNo = lstIppsnaps.AccountNumber,
                                                             providerID = lstProviders.HospitalDefaultsValue,
                                                             MeasureID = lstMeasures.MeasureSetID,
                                                             AnswerCode = lstCDatas.DataValue,
                                                             Questioncode = lstCDatas.DataKey
                                                         }
                                          };




                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                    }
                    else
                    {
                        int count = patientdatalist.Count();
                        //This outer loop displays Patient List
                        foreach (var providerlst in patientdatalist)
                        {
                            int Patientcount = providerlst.patients.ToList().Count();
                            Provider provider = new Provider();
                            provider.Providerid = providerlst.providerID;
                            //Creation of List of Patients specific to Provider
                            provider.Patient = new List<Patient>();
                            foreach (var patientlst in providerlst.patients)
                            {
                                checkUnknownMeasureSet = false;
                                checkMultipleMeasureSet = false;

                                var MeasureSetValue = Acc_MeasureSet_Collection.Where(m => m.Key == patientlst.IppKey.ToString()).Select(m => m.Value).FirstOrDefault();
                                if (patientlst.MeasureID != MeasureSetValue)
                                {
                                    checkMultipleMeasureSet = true;
                                }

                                if (checkMultipleMeasureSet == true)
                                    continue;

                                checkPthic = false;
                                //Object creation of serialized classes
                                Patient allpatients = new Patient();
                                allpatients.Encounter = new Encounter();
                                Encounter encounter = new Encounter();

                                //Code for Mapping database fields with XML serialized classes
                                switch (patientlst.MeasureID)
                                {
                                    case "OP-AMI":
                                        encounter.Measureset = "HOP-AMI";
                                        break;
                                    case "OP-CHEST PAIN":
                                        encounter.Measureset = "HOP-CHEST PAIN";
                                        break;
                                    case "OP-ED":
                                        encounter.Measureset = "ED-THROUGHPUT";
                                        break;
                                    case "OP-STK":
                                        encounter.Measureset = "STROKE";
                                        break;
                                    case "OP-PAIN":
                                        encounter.Measureset = "PAIN MANAGEMENT";
                                        break;
                                    case "OP-WB-30":
                                        encounter.Measureset = "ENDPC_O";
                                        break;
                                    case "OP-WB-29":
                                        encounter.Measureset = "ENDFC_O";
                                        break;
                                    case "OP-WB-31":
                                        encounter.Measureset = "CATRCT_O";
                                        break;
                                    case null:
                                        encounter.Measureset = "UNKNOWN";
                                        break;
                                }

                                encounter.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                                encounter.Hospitalpatientid = patientlst.UnitNo;
                                encounter.Medicalrecordnumber = patientlst.AccNo;

                                //To generate HIC number
                                Detail detail = new Detail();
                                if (patientlst.Questioncode == "MS_Payment")
                                {
                                    encounter.pthic = patientlst.AnswerCode.Substring(patientlst.AnswerCode.LastIndexOf('~') + 1);
                                }

                                if (encounter.pthic == null && checkPthic == false)
                                {
                                    checkPthic = true;
                                }
                                if (checkPthic)
                                    continue;
                                allpatients.Encounter = encounter;
                                provider.Patient.Add(allpatients);                               
                            }
                            if (Patientcount != 0)
                            {
                                submission.Provider.Add(provider);
                            }
                        }
                        XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                        XmlAttributes attrs = new XmlAttributes();

                        /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                           applied to the Comment field. Thus it will be serialized.*/
                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Encounter), "pthic", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Encounter), "arrival_time", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "birthdate", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "sex", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "race", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "ethnic", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "attesting_physician_code", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "npi", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "attesting_physician_specialty_code", attrs);


                        //Call to 'GenerateXml' Method for Serialization
                        XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                        XmlFilePath ="HICOPPS";
                        XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters); 
                    }
                }
                _log.Info("Exit for Export method of HIC OPPS Measure");
                return new ExportResult() { IsValid = true };   
            }
            catch (Exception ex)
            {
                 _log.Error("Error occured while exporting HIC OPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
