﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;

namespace VSMMefMvc.BusinessRules.OPPSExport
{
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "OPPS-CAC")]
    public class CACOPPSExport : IExportService
    {
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }

        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
