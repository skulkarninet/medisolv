﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;


namespace ExportPlugins.OPPSExport
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'CATARACTS (CATRCT_O)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "OPPS-CATRCTO")]
    public class CATRCT_OOPPSExport : IExportService
    {
         Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="CATRCT_OOPPSExport"/> class.
        /// </summary>
        public CATRCT_OOPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        ExportPlugins.DataMapper.OPPSDataMapper OPPSdataMapping = new ExportPlugins.DataMapper.OPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of PM MeasureSet
        /// </summary>   
        ///   
          public ExportResult ExportXML(ExportParameters exportParameters)
        {
            //Database access through entity framework
            try
            {
                _log.Info("Entered Export XML for CATRCT_OOPPS");
                //Object of Submission class to store result of 'DataFetch' method 
                Submission submission = new Submission();

                //Call to 'DataFetch' Method 
                submission = OPPSdataMapping.OPPSDataFetch(exportParameters, Constants.OPCataracts, "CATRCTO");

                //If Patient data is not available 
                if (submission == null)
                    return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                //If Patient data is available
                else
                {
                    XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                    XmlAttributes attrs = new XmlAttributes();

                    /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                       applied to the Comment field. Thus it will be serialized.*/
                    attrs.XmlIgnore = true;
                    xOver.Add(typeof(Encounter), "pthic", attrs);

                    //attrs.XmlIgnore = false;
                    //xOver.Add(typeof(Encounter), "Detail", attrs);
                    //Call to 'GenerateXml' Method for Serialization
                    XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                    XmlFilePath = "CATRCT_OOPPS";
                    XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters);

                    _log.Info("Exit for Export method of CATRCT_O OPPS Measure");
                    return new ExportResult() { IsValid = true };
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting CATRCT_O OPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = ex.ToString() };
            }   
        }

        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
