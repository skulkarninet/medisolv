﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExportPlugins.DataMapping
{
    /// <summary>
    /// This class hold the constants defined and the values are used
    /// across all the plug ins.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Constant values for Submission as per the requirement.
        /// </summary>      
        public static string SubmissionType = "HOSPITAL";
        public static string SubmissionData = "CLINICAL";
        public static string SubmissionVersion = "1.0";
        public static string SubmissionActioncode = "ADD";

        /// <summary>
        /// Hardcoded ProviderId to 1 
        /// </summary>
        public static string SubmissionProviderid = "1";

        /// <summary>
        /// Hardcoding values for query
        /// </summary>    
        public static string AmiMeasureset = "AMI";
        public static string HfMeasureset = "HF";
        public static string CacMeasureset = "CAC";
        public static string VteMeasureset = "VTE";
        public static string StkMeasureset = "STK";

        /// <summary>
        /// Hardcoded Specification to 'I' for IPPS
        /// </summary>      
        public static string InpatientSpecification = "I";

        /// <summary>
        /// Hardcoding value for excluding questioncodes starting with "MS_"
        /// </summary>             
        public static string QuestionCodeMs = "MS_";
    }
}
