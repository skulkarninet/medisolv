﻿using System.Xml;
using System.Xml.Serialization;
using  ExportPlugins.Model;

namespace ExportPlugins.DataMapping
{
    public static class Serialization
    {
        public static void GenerateXml(string xmlFilePath, object submission)
        {
            //Serialization for XML export
            XmlSerializer serializer = new XmlSerializer(typeof(Submission));
            using (XmlWriter writer = XmlWriter.Create(xmlFilePath))
            {
                XmlSerializerNamespaces serializerNamespace = new XmlSerializerNamespaces();
                serializerNamespace.Add("", "");
                serializer.Serialize(writer, submission, serializerNamespace);
            }
        }
    }
}
