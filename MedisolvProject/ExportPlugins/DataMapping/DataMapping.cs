﻿using System;
using System.Collections.Generic;
using System.Linq;
using EncorExportWebUI.Models;
using System.Data.Entity;
using ExportPlugins.DataAccess;
using  ExportPlugins.Model;

namespace ExportPlugins.DataMapping
{
    /// <summary>
    /// This class Fetches data from Database with Entity Framework 4.0 and maps it to XML Serialization Classes 
    /// </summary>
    public class DataMapping
    {
        //RowCount 
        int _rowNoCount = 0;

        /// <summary>
        /// This Method Fetches data with passed exportParameters() and executes for selectedMeasureSet 
        /// </summary>
        public Submission DataFetch(ExportParameters exportParameters,string selectedMeasureset)
        {
            //Code for Object creation of serialized classes
            Submission submission = new Submission();
            submission.Provider = new Provider();
            Provider provider = new Provider();

            //Code for Database access through Entity Framework 4.0
            using (RapidEntities1 context = new RapidEntities1())
            {           
                //Creation of List of Patients specific to Provider
                provider.Patient = new List<Patient>();

                //Access database tables
                DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                DbSet<CaseData> casedatas = context.CaseDatas;
                DbSet<MeasureSet> measureSets = context.MeasureSets;
                DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;

                //Constant values for unavailable mapping fields of Submission Class
                provider.Providerid = Constants.SubmissionProviderid;
                submission.Type = Constants.SubmissionType;
                submission.Data = Constants.SubmissionData;
                submission.Version = Constants.SubmissionVersion;
                submission.Actioncode = Constants.SubmissionActioncode;

                //This Query access Patient Data with its Details(Question_cd,Answer_cd)
                var patientdatalist = from lstPatients in initialPatientPopulations
                    join lstMeasures in measureSets
                        on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                    join lstVersions in versions
                        on lstMeasures.VersionKey equals lstVersions.VersionKey
                    join lstIppsnaps in ippSnapshots
                        on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                    orderby lstIppsnaps.AccountNumber
                    where
                        exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                        lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                        lstMeasures.MeasureSetID == selectedMeasureset &&
                        lstIppsnaps.InpatientOrOutpatient == Constants.InpatientSpecification
                    select new
                    {
                        IppKey = lstPatients.IPPKey,
                        Adate = lstIppsnaps.AdmitDateTime,
                        Ddate = lstIppsnaps.DischargeDateTime,
                        UnitNo = lstIppsnaps.UnitNumber,
                        AccNo = lstIppsnaps.AccountNumber,
                        MeasureID = lstMeasures.MeasureSetID,
                        Casedata = from lstCDatas in casedatas
                            where
                                lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                                !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)
                            select new
                            {
                                AnswerCode = lstCDatas.DataValue,
                                Questioncode = lstCDatas.DataKey
                            }
                    };


                //Check if Patient Data is availble between StartDate and EndDate
                if (!patientdatalist.Any())
                {
                    return null;
                }
                else
                {
                    //This outer loop displays Patient List
                    foreach (var patientlst in patientdatalist)
                    {
                        int count=patientdatalist.Count();
                        //Object creation of serialized classes
                        Patient allpatients = new Patient();
                        allpatients.Episodeofcare = new Episodeofcare();
                        Episodeofcare episodeOfCare = new Episodeofcare();
                        episodeOfCare.Detail = new List<Detail>();

                        //List of QuestionCodes for generating RowCount
                        List<string> lstQuestioncd = new List<string>();

                        //Code for Mapping database fields with XML serialized classes
                        episodeOfCare.Measureset = patientlst.MeasureID;
                        episodeOfCare.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                        episodeOfCare.Dischargedate = ((Convert.ToDateTime(patientlst.Ddate)).Date).ToString("MM-dd-yyyy");
                        episodeOfCare.Hospitalpatientid = patientlst.UnitNo;
                        episodeOfCare.Medicalrecordnumber = patientlst.AccNo;

                        //This inner loop displays Details(Question_cd,Answer_cd) of respective Patient from outer loop
                        foreach (var casedatalst in patientlst.Casedata)
                        {
                            Detail detail = new Detail();

                            //Code for Datetime parsing
                            DateTime dateValue;
                            if (DateTime.TryParse(casedatalst.AnswerCode, out dateValue))
                            {
                                if (casedatalst.AnswerCode.Length > 10)
                                    detail.Answercode =
                                        ((Convert.ToDateTime(casedatalst.AnswerCode)).Date).ToString("MM-dd-yyyy");
                                else
                                    detail.Answercode = casedatalst.AnswerCode;
                            }
                            else
                            {
                                detail.Answercode = casedatalst.AnswerCode;
                                detail.Answercode = detail.Answercode.Replace(",", string.Empty);
                            }
                            detail.Questioncd = casedatalst.Questioncode;

                            //Code for Generating Rowcount                                   
                            if (lstQuestioncd.Contains(detail.Questioncd))
                            {
                                _rowNoCount = lstQuestioncd.Count();
                            }

                            lstQuestioncd.Add(detail.Questioncd);
                            detail.Rownumber = (_rowNoCount).ToString();
                            episodeOfCare.Detail.Add(detail);

                            //Initiliazing RowCount to Zero
                            _rowNoCount = 0;
                        }
                        allpatients.Episodeofcare = episodeOfCare;
                        provider.Patient.Add(allpatients);
                        submission.Provider = provider;
                    }
                }
            }
            return submission;
        }
    }
}
    
