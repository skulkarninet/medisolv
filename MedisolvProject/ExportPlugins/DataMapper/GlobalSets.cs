﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using EncorExportWebUI.Models;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Text.RegularExpressions;
using ExportPlugins.DataAccess;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.DataMapper
{
    /// <summary>
    /// This class fetches subsets of GLOBAL,PC and IPF MeasureSets
    /// </summary>
    public class GlobalSets
    {
        Logger _log;
        public GlobalSets()
        {
            _log = Container.ResolveLoggerInstance();
        }

        public string measureSet;
        /// <summary>
        /// This Method Fetches data with Questioncode and Measureset from CompleteDataa,DemographicData,HIC from Global Sets and returns MeasureSet
        /// </summary>
        public string GlobalDataFetch(string casedatalstQuestioncode,string patientlstMeasureID,bool flag2)
        {
            try
            {
                _log.Info("Entered GlobalDataFetch Method of GlobalSets");

                if (patientlstMeasureID == "GLOBAL" && flag2 == false)
                {
                    if (casedatalstQuestioncode == "ARRVLDATE" ||
                        casedatalstQuestioncode == "ARRVLTIME" ||
                        casedatalstQuestioncode == "DCNADMITDT" ||
                        casedatalstQuestioncode == "DCNADMITTM" ||
                        casedatalstQuestioncode == "EDDEPARTDT" ||
                        casedatalstQuestioncode == "EDDEPARTTM" ||
                        casedatalstQuestioncode == "EDPATIENT" ||
                        casedatalstQuestioncode == "HOSPADMIN" ||
                        casedatalstQuestioncode == "PHYSICIAN_1" ||
                        casedatalstQuestioncode == "PHYSICIAN_2")
                    {

                        measureSet = "ED";
                        flag2 = true;

                    }
                    else if (casedatalstQuestioncode == "PNEVACSTATUS" ||
                             casedatalstQuestioncode == "FLUVACSTATUS" ||
                             casedatalstQuestioncode == "HOSPADMIN" ||
                             casedatalstQuestioncode == "PHYSICIAN_1" ||
                             casedatalstQuestioncode == "PHYSICIAN_2")
                    {
                        measureSet = "IMM";
                        flag2 = true;

                    }
                    else if (casedatalstQuestioncode == "HOSPADMIN" ||
                             casedatalstQuestioncode == "ALCDRGDISORD" ||
                             casedatalstQuestioncode == "ALCSTATUS" ||
                             casedatalstQuestioncode == "BRFINTVTN" ||
                             casedatalstQuestioncode == "FUCONTACT" ||
                             casedatalstQuestioncode == "FUCONTACTDT" ||
                             casedatalstQuestioncode == "REFADDTX" ||
                             casedatalstQuestioncode == "RXALCDRGMED" ||
                             casedatalstQuestioncode == "ALCODRUGPOSTDISCH" ||
                             casedatalstQuestioncode == "ALCSTATDCCOUN" ||
                             casedatalstQuestioncode == "ALCSTATDCMED" ||
                             casedatalstQuestioncode == "ALCSTATDCQUIT" ||
                             casedatalstQuestioncode == "DRGSTATDCQUIT" ||
                             casedatalstQuestioncode == "PHYSICIAN_1" ||
                             casedatalstQuestioncode == "PHYSICIAN_2" ||
                             casedatalstQuestioncode == "COMFORTMX")
                    {
                        measureSet = "SUB";
                        flag2 = true;

                    }
                    else if ((casedatalstQuestioncode == "HOSPADMIN" ||
                              casedatalstQuestioncode == "REFOPTOBCSNG" ||
                              casedatalstQuestioncode == "TOBSTATUS" ||
                              casedatalstQuestioncode == "RXTOBMED" ||
                              casedatalstQuestioncode == "FUCONTACT" ||
                              casedatalstQuestioncode == "FUCONTACTDT" ||
                              casedatalstQuestioncode == "TOBTXCOUNS" ||
                              casedatalstQuestioncode == "TOBTXMED" ||
                              casedatalstQuestioncode == "TOBUSEPOSTDISCH" ||
                              casedatalstQuestioncode == "RSNNOTOBDC" ||
                              casedatalstQuestioncode == "RSNNOTOBSTAY" ||
                              casedatalstQuestioncode == "TOBSTATDCCOUN" ||
                              casedatalstQuestioncode == "TOBSTATDCMED" ||
                              casedatalstQuestioncode == "TOBSTATDCQUIT" ||
                              casedatalstQuestioncode == "PHYSICIAN_1" ||
                              casedatalstQuestioncode == "PHYSICIAN_2" ||
                              casedatalstQuestioncode == "COMFORTMX"))
                    {

                        measureSet = "TOB";
                        flag2 = true;

                    }
                }
                else if (patientlstMeasureID == "PC" && flag2 == false)
                {
                    if (casedatalstQuestioncode == "TERMNB" ||
                        casedatalstQuestioncode == "CLNCLTRIAL" ||
                        casedatalstQuestioncode == "EXCBRTFED" ||
                        casedatalstQuestioncode == "ADMSNNICU" ||
                        casedatalstQuestioncode == "HOSPADMIN" ||
                        casedatalstQuestioncode == "PHYSICIAN_1" ||
                        casedatalstQuestioncode == "PHYSICIAN_2")
                    {

                        measureSet = "PC_BF";
                        flag2 = true;

                    }
                    else if (casedatalstQuestioncode == "BIRTHWEIGHT" ||
                             casedatalstQuestioncode == "BLDINFCFMD" ||
                             casedatalstQuestioncode == "BLDINFPRSADM" ||
                             casedatalstQuestioncode == "CLNCLTRIAL" ||
                             casedatalstQuestioncode == "HOSPADMIN" ||
                             casedatalstQuestioncode == "PHYSICIAN_1" ||
                             casedatalstQuestioncode == "PHYSICIAN_2")
                    {

                        measureSet = "PC_BSI";
                        flag2 = true;

                    }
                    else if (casedatalstQuestioncode == "CLNCLTRIAL" ||
                             casedatalstQuestioncode == "ACTLABOR" ||
                             casedatalstQuestioncode == "GESTAGE" ||
                             casedatalstQuestioncode == "PARITY" ||
                             casedatalstQuestioncode == "ANTSTADM" ||
                             casedatalstQuestioncode == "RSNNOANTST" ||
                             casedatalstQuestioncode == "PRUTRNSURG" ||
                             casedatalstQuestioncode == "HOSPADMIN" ||
                             casedatalstQuestioncode == "PHYSICIAN_1" ||
                             casedatalstQuestioncode == "PHYSICIAN_2")
                    {

                        measureSet = "PCM";
                        flag2 = true;

                    }
                }
                else if (patientlstMeasureID == "HBIPS-GLOBAL" && flag2 == false)
                {
                    if (casedatalstQuestioncode == "FLUVACSTATUS" ||
                        casedatalstQuestioncode == "HOSPADMIN" ||
                        casedatalstQuestioncode == "PHYSICIAN_1" ||
                        casedatalstQuestioncode == "PHYSICIAN_2")
                    {

                        measureSet = "IPFIMM";
                        flag2 = true;

                    }
                    else if (casedatalstQuestioncode == "HOSPADMIN" ||
                             casedatalstQuestioncode == "ALCSTATUS" ||
                             casedatalstQuestioncode == "PHYSICIAN_1" ||
                             casedatalstQuestioncode == "PHYSICIAN_2" ||
                             casedatalstQuestioncode == "COMFORTMX")
                    {

                        measureSet = "IPFSUB";
                        flag2 = true;

                    }
                    else if (casedatalstQuestioncode == "HOSPADMIN" ||
                             casedatalstQuestioncode == "TOBSTATUS" ||
                             casedatalstQuestioncode == "TOBTXCOUNS" ||
                             casedatalstQuestioncode == "TOBTXMED" ||
                             casedatalstQuestioncode == "RSNNOTOBSTAY" ||
                             casedatalstQuestioncode == "PHYSICIAN_1" ||
                             casedatalstQuestioncode == "PHYSICIAN_2" ||
                             casedatalstQuestioncode == "COMFORTMX")
                    {
                        measureSet = "IPFSUB";
                        flag2 = true;
                    }
                }
                else if (flag2 == false &&
                         (patientlstMeasureID == "AMI" || patientlstMeasureID == "CAC" ||
                          patientlstMeasureID == "SCIP" || patientlstMeasureID == "VTE" ||
                          patientlstMeasureID == "STK" || patientlstMeasureID == "SEP"))
                {
                    measureSet = patientlstMeasureID;
                    flag2 = true;
                }
                else if (flag2 == false)
                {
                    measureSet = "UNKNOWN";
                    flag2 = true;
                }
                _log.Info("Exit for GlobalDataFetch Method of GlobalSets");
                return measureSet;
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while executing Global DataFetch Method of GlobalSets" + ex.Message);
                return null;
            }
        }
    }
}

