﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using EncorExportWebUI.Models;
using ExportPlugins.IPPSExport;
using ExportPlugins.Model;
using TracerX;


namespace ExportPlugins.DataMapper
{
    /// <summary>
    /// Class Serialization.
    /// </summary>
    public static class XMLSerializer
    {
        /// <summary>
        ///This Method Generates XML.
        /// </summary>
        public static void GenerateXml<T>(string xmlFilePath, T submission, XmlAttributeOverrides xOver,
            ExportParameters exportParameters)
        {
            try
            {
                string date = DateTime.Now.ToString("ddd MM.dd.yyyy");
                string time = DateTime.Now.ToString("HH.mm.ss");
                string folderName = date + "-"+time;
              
                string fileName = xmlFilePath + folderName + ".xml";
                string directoryPath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                //Getting zip path from Config for Multiple File Export on D:
                string zipPath = System.Configuration.ConfigurationManager.AppSettings["ZipPath"];
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                if (!Directory.Exists(zipPath))
                    Directory.CreateDirectory(zipPath);
            
                string xmlSavePath = string.Empty;
               //Checks multiselected MeasureSets for Zip creation.
                if (exportParameters.IsMultipleSelect)
                {
                    xmlSavePath = Path.Combine(directoryPath, fileName);

                }
                else
                {
                    xmlSavePath = Path.Combine(zipPath, fileName);
                }

                //Getting file path from Config for single File Export on D:
                
                //Serialization for XML export
                XmlSerializer serializer = new XmlSerializer(typeof(T), xOver);
                using (XmlWriter writer = XmlWriter.Create(xmlSavePath))
                {
                    XmlSerializerNamespaces serializerNamespace = new XmlSerializerNamespaces();
                    serializerNamespace.Add("", "");
                    serializer.Serialize(writer, submission, serializerNamespace);
                }

                //Checks multiselected MeasureSets for Zip creation.
                if (exportParameters.IsMultipleSelect && exportParameters.isLastPlugin)
                {
                    ZipFile.CreateFromDirectory(directoryPath, Path.Combine(zipPath,folderName+".zip"));
                    if(Directory.Exists(directoryPath))
                        Directory.Delete(directoryPath,true);
                }

            }
            catch (Exception)
            {                                
            }
        }
     }
           
    }
    

