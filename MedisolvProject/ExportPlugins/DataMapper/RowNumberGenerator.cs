﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.DataMapper
{
    /// <summary>
    /// Class RowNumberGenerator.
    /// </summary>
    public class RowNumberGenerator
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="RowNumberGenerator"/> class.
        /// </summary>
        public RowNumberGenerator()
        {
            _log = Container.ResolveLoggerInstance();
        }

        /// <summary>
        /// Gets the row no.
        /// </summary>
        /// <param name="questionCd">The question cd.</param>
        /// <param name="answerCd">The answer cd.</param>
        /// <returns>List&lt;System.String&gt;.</returns>
        public List<string> GetRowNo(string questionCd, string answerCd)
        {
            try
            {
                _log.Info("Entered GetRowNo Method of RowNumberGenerator");

                List<string> answerCdList = new List<string>();
                bool flag;
                DateTime dateValue;
                List<string> splitList = new List<string>();

                switch (questionCd)
                {
                    case "CPTCategoryIModifiers":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('|').ToList();
                            foreach (var word in splitList)
                            {
                                int l = word.IndexOf("~");
                                if (word.Contains("~"))
                                {
                                    if (l > 0)
                                    {
                                        answerCdList.Add(word.Substring(0, l));
                                    }
                                }
                            }
                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;
                    case "CPTCodes":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('|').ToList();
                            foreach (var word in splitList)
                            {
                                int l = word.IndexOf("~");
                                if (word.Contains("~"))
                                {
                                    if (l > 0)
                                    {
                                        if (!word.Contains("Not Available"))
                                            answerCdList.Add(word.Substring(0, l));
                                    }
                                }
                                else if (!word.Contains("Not Available"))
                                {
                                    answerCdList.Add(word);
                                }
                            }

                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;
                    case "ICUVTEPROPH":
                    case "VTEPROPH":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split(';').ToList();
                            foreach (var word in splitList)
                            {
                                flag = word.Contains("True");
                                if (flag == true)
                                {
                                    answerCdList.Add((word.Substring(0, word.IndexOf('~'))).Replace(",", string.Empty));
                                }
                            }
                        }
                        else if(answerCd!= string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;
                    case "VANCO":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('|').ToList();
                        foreach (var word in splitList)
                        {
                                answerCdList.Add(word);
                        }
                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;

                    case "OTHRDX#":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('|').ToList();
                        foreach (var word in splitList)
                        {
                            int SequenceIndex = word.IndexOf(",");
                            if (SequenceIndex > 0)
                            {
                                answerCdList.Add(word.Substring(SequenceIndex + 1));
                            }
                        }
                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                break;

                    case "PREOPHRREM":
                    case "REASONCNTCATH":
                    case "RSNEXTABX":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('~').ToList();
                        foreach (var word in splitList)
                        {
                            flag = word.Contains("True");
                            if (flag == true)
                            {
                                answerCdList.Add((word.Substring(0, word.IndexOf('='))).Replace(",", string.Empty));
                                for (int i = 0; i < answerCdList.Count; i++)
                                {
                                    switch (answerCdList[i])
                                    {
                                        case "One":
                                            answerCdList[i] = "1";
                                            break;
                                        case "Two":
                                            answerCdList[i] = "2";
                                            break;
                                        case "Three":
                                            answerCdList[i] = "3";
                                            break;
                                        case "Four":
                                            answerCdList[i] = "4";
                                            break;
                                        case "Five":
                                            answerCdList[i] = "5";
                                            break;
                                        case "Six":
                                            answerCdList[i] = "6";
                                            break;
                                        case "Seven":
                                            answerCdList[i] = "7";
                                            break;
                                        case "Eight":
                                            answerCdList[i] = "8";
                                            break;
                                        case "Nine":
                                            answerCdList[i] = "9";
                                            break;

                                    }
                                }
                            }
                        }
                       }
                        else if (answerCd != string.Empty)
                        {
                answerCdList.Add(answerCd);
            }
            break;

                    case "BBLKRPERIOP":
                    case "CTRBBLKPERIOP":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('~').ToList();
                            foreach (var word in splitList)
                            {
                                flag = word.Contains("True");
                                if (flag == true)
                                {
                                    answerCdList.Add((word.Substring(0, word.IndexOf('='))).Replace(",", string.Empty));
                                }
                            }
                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;
                    case "OTHRPX#":
                        if (answerCd.Length > 1)
                        {
                            splitList = answerCd.Split('|').ToList();
                            foreach (var word in splitList)
                            {
                                int l = word.IndexOf("~");
                                if (word.Contains("~"))
                                {
                                    if (l > 0)
                                    {
                                        if (word.Substring(0, l) != "UTD" && word.Substring(0, l) != "M")
                                            answerCdList.Add(word.Substring(0, l));
                                    }
                                }
                                else if (!word.Contains("UTD") && !word.Contains("M"))
                                {
                                    answerCdList.Add(word);
                                }
                            }
                            
                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;
                    case "PRINPX":
                        if (answerCd.Length > 1)
                        {
                            int l = answerCd.IndexOf("~");
                            if (answerCd.Contains("~"))
                            {
                                if (l > 0)
                                {
                                    if (answerCd.Substring(0, l) != "UTD" && answerCd.Substring(0, l) != "M")
                                        answerCdList.Add(answerCd.Substring(0, l));
                                }
                            }
                            else if (!answerCd.Contains("UTD") && !answerCd.Contains("M"))
                            {
                                answerCdList.Add(answerCd);
                            }
                        }
                        else if (answerCd != string.Empty)
                        {
                            answerCdList.Add(answerCd);
                        }
                        break;
                    default:
                        if (DateTime.TryParse(answerCd, out dateValue))
                        {
                            if (answerCd.Length > 10)
                            {
                                answerCd = ((Convert.ToDateTime(answerCd)).Date).ToString("MM-dd-yyyy");
                                answerCdList.Add(answerCd);
                            }
                            else
                            {
                                answerCdList.Add(answerCd);
                            }
                        }
                        else if(answerCd!=string.Empty)
                        {
                            answerCd = answerCd.Replace(",", string.Empty);
                            answerCdList.Add(answerCd);
                        }
                        break;
                }
                return answerCdList;
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while executing GetRowNo Method of RowNumberGenerator" + ex.Message);
                return null;
            }
        }  
}
}
