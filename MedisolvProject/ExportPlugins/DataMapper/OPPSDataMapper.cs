﻿using System;
using System.Collections.Generic;
using System.Linq;
using EncorExportWebUI.Models;
using System.Data.Entity;
using ExportPlugins.DataAccess;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.DataMapper
{
    /// <summary>
    /// This class Fetches data from Database with Entity Framework 4.0 and maps it to XML Serialization Classes for OPPS Export
    /// </summary>
    public class OPPSDataMapper
    {
        Logger _log;
        public OPPSDataMapper()
        {
            _log = Container.ResolveLoggerInstance();
        }

        /// <summary>
        /// This Method Fetches data with passed exportParameters() and executes for selectedMeasureSet 
        /// </summary>
        public Submission OPPSDataFetch(ExportParameters exportParameters, string selectedMeasureset,string check)
        {
            try
            {
                _log.Info("Entered OPPSDataFetch Method of OPPSDataMapper");


                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();               

                var measures = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == check)).Select(m => m.AccountNumber).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == check)).Select(m => m.IPPKey).ToList();


                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {
                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //Constant values for unavailable mapping fields of Submission Class
                    submission.Type = Constants.SubmissionTypeOutpatient;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)                   
                    var patientdatalist = from Providerslst in hospitalDefaults
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID"
                                          select new
                                          {
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              patients = from lstPatients in initialPatientPopulations
                                                         join lstMeasures in measureSets
                                                             on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                                         join lstVersions in versions
                                                             on lstMeasures.VersionKey equals lstVersions.VersionKey
                                                         join lstIppsnaps in ippSnapshots
                                                             on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                                         join lstProviders in hospitalDefaults
                                                             on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                                         where
                                                         exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                         lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                         lstMeasures.MeasureSetID == selectedMeasureset &&
                                                         lstIppsnaps.InpatientOrOutpatient == Constants.Specification.O.ToString() &&
                                                         lstProviders.HospitalDefaultName == "ProviderID" &&
                                                         lstIppsnaps.HospitalID == Providerslst.HospitalID &&
                                                         ((measures.Contains(lstIppsnaps.AccountNumber)) && (ippkeys.Contains(lstIppsnaps.IPPIdentity.ToString()))) &&
                                                          ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null && exportParameters.SelectedHospID == null))
                                                         select new
                                                         {
                                                             IppKey = lstPatients.IPPKey,
                                                             Adate = lstIppsnaps.AdmitDateTime,
                                                             Ddate = lstIppsnaps.DischargeDateTime,
                                                             UnitNo = lstIppsnaps.UnitNumber,
                                                             AccNo = lstIppsnaps.AccountNumber,
                                                             MeasureID = lstMeasures.MeasureSetID,
                                                             providerID = lstProviders.HospitalDefaultsValue,
                                                             Casedata = from lstCDatas in casedatas
                                                                        where
                                                                            lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                                                                            !(lstCDatas.DataKey.Contains(Constants.QuestionCodeMs))
                                                                        select new
                                                                        {
                                                                            AnswerCode = lstCDatas.DataValue,
                                                                            Questioncode = lstCDatas.DataKey
                                                                        }
                                                         }
                    };                                                                                    
                                          

                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return null;
                    }
                    else
                    {
                        int count = patientdatalist.Count();
                        //This outer loop displays Patient List
                        foreach (var providerlst in patientdatalist)
                        {
                            int Patientcount = providerlst.patients.ToList().Count();
                            Provider provider = new Provider();
                            provider.Providerid = providerlst.providerID;
                            provider.Patient = new List<Patient>();
                            foreach (var patientlst in providerlst.patients)
                            {
                                //Object creation of serialized classes
                              
                                Patient allpatients = new Patient();
                                allpatients.Encounter = new Encounter();
                                Encounter encounter = new Encounter();
                                encounter.Detail = new List<Detail>();

                                //List of QuestionCodes for generating RowCount
                                List<string> lstQuestioncd = new List<string>();

                                //Switch for OPPS MeasureSets

                                switch (patientlst.MeasureID)
                                {
                                    case "OP-AMI":
                                        encounter.Measureset = "HOP-AMI";
                                        break;
                                    case "OP-CHEST PAIN":
                                        encounter.Measureset = "HOP-CHEST PAIN";
                                        break;
                                    case "OP-ED":
                                        encounter.Measureset = "ED-THROUGHPUT";
                                        break;
                                    case "OP-STK":
                                        encounter.Measureset = "STROKE";
                                        break;
                                    case "OP-PAIN":
                                        encounter.Measureset = "PAIN MANAGEMENT";
                                        break;
                                    case "OP-WB-30":
                                        encounter.Measureset = "ENDPC_O";
                                        break;
                                    case "OP-WB-29":
                                        encounter.Measureset = "ENDFC_O";
                                        break;
                                    case "OP-WB-31":
                                        encounter.Measureset = "CATRCT_O";
                                        break;
                                }

                                encounter.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                                encounter.Hospitalpatientid = patientlst.UnitNo;
                                encounter.Medicalrecordnumber = patientlst.AccNo;
                                if (exportParameters.SelectedAction == "ADD")
                                {
                                    //This inner loop displays Details(Question_cd,Answer_cd) of respective Patient from outer loop
                                    foreach (var casedatalst in patientlst.Casedata)
                                    {
                                        List<string> answerList = new List<string>();
                                        RowNumberGenerator obj = new RowNumberGenerator();

                                        int chkcount = 0;
                                        //RowNumber Logic
                                        answerList = obj.GetRowNo(casedatalst.Questioncode, casedatalst.AnswerCode);
                                        foreach (var answer in answerList)
                                        {
                                            Detail detail = new Detail();
                                            detail.Questioncd = casedatalst.Questioncode;
                                            detail.Answercode = answer;
                                            detail.Rownumber = chkcount.ToString();
                                            chkcount++;
                                            encounter.Detail.Add(detail);
                                        }
                                    }
                                }
                                allpatients.Encounter = encounter;
                                provider.Patient.Add(allpatients);
                            }
                            if (Patientcount != 0)
                            {
                                submission.Provider.Add(provider);
                            }
                        }
                    }
                }
                _log.Info("Exit for OPPSDataFetch Method of OPPSDataMapper");
                return submission;
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while executing OPPSDataFetch Method of OPPSDataMapper" + ex.Message);
                return null;
            }
        }
    }
}

