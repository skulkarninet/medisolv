﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EncorExportWebUI;
using EncorExportWebUI.ViewModels;

namespace ExportPlugins.DataMapper
{
    /// <summary>
    /// This class hold the constants defined and the values are used
    /// across all the plug ins.
    /// </summary>
    public static class Constants
    {      
        /// <summary>
        /// Hardcoded values as per the requirement for submission.
        /// </summary>
        ///
        public static string SubmissionType = "HOSPITAL";
        /// <summary>
        /// The submission data
        /// </summary>
        public static string SubmissionData = "CLINICAL";
        /// <summary>
        /// The submission type outpatient
        /// </summary>
        public static string SubmissionTypeOutpatient = "OUTPATIENT";
        /// <summary>
        /// The submission version
        /// </summary>
        public static string SubmissionVersion = "1.0";


        /// <summary>
        /// Enum Enummeasureset
        /// </summary>
        public enum Enummeasureset
        {
            AMI, HF, SCIP, CAC, HBIPS, IMM, PCM, PC_BF, PC_BSI, ED, SUB, TOB, VTE, STK, IPF_SUB, IPF_TOB, IPF_IMM, SEP

        }
        /// <summary>
        /// The opami
        /// </summary>
        public static string OPAMI= "OP-AMI";
        /// <summary>
        /// The opchestpain
        /// </summary>
        public static string OPCHESTPAIN = "OP-CHEST PAIN";
        /// <summary>
        /// The oped
        /// </summary>
        public static string OPED = "OP-ED";
        /// <summary>
        /// The oppain
        /// </summary>
        public static string OPPAIN = "OP-PAIN";
        /// <summary>
        /// The opstk
        /// </summary>
        public static string OPSTK = "OP-STK";
        /// <summary>
        /// The op cataracts
        /// </summary>
        public static string OPCataracts = "OP-WB-31";
        /// <summary>
        /// The opendf c_ o
        /// </summary>
        public static string OPENDPC_O = "OP-WB-30";
        /// <summary>
        /// The opendp c_ o
        /// </summary>
        public static string OPENDFC_O = "OP-WB-29";

        /// <summary>
        /// Enum Specification
        /// </summary>
        public enum Specification
        {
           I,O
        }      

        //Hardcoding value for excluding questioncodes starting with "MS_" 
        public static string QuestionCodeMs = "MS_";
    }
}
