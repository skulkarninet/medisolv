﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using EncorExportWebUI.Models;
using System.Data.Entity;
using System.Data.Entity.Core.Mapping;
using System.Text.RegularExpressions;
using ExportPlugins.DataAccess;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.DataMapper
{
    /// <summary>
    /// This class Fetches data from Database with Entity Framework 4.0 and maps it to XML Serialization Classes for IPPS Export.
    /// </summary>
    public class IPPSDataMapper
    {
        Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="IPPSDataMapper"/> class.
        /// </summary>
        public IPPSDataMapper()
        {
            _log = Container.ResolveLoggerInstance();
        }
        public bool checkMultipleMeasureSet = false;
        /// <summary>
        /// This Method Fetches data with passed exportParameters() and executes for selectedMeasureSet 
        /// </summary>
        public Submission DataFetch(ExportParameters exportParameters,string selectedMeasureset,string check)
        {
            try
            {
                _log.Info("Entered DataFetch Method of DataMapper");

                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();

                //Gets selected Account number and MeasureSet from Grid
                var measures = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == check)).Select(m => m.AccountNumber).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export== check)).Select(m => m.IPPKey).ToList();
                
                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {
                    //Creation of List of Patients specific to Provider

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;
                                 
                    //Constant values for unavailable mapping fields of Submission Class
                    submission.Type = Constants.SubmissionType;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)                   
                    var patientdatalist = from Providerslst in hospitalDefaults                                        
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID"                                                                                            
                                          select new
                                          {
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              patients= from lstPatients in initialPatientPopulations
                                                        join lstMeasures in measureSets
                                                            on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                                        join lstVersions in versions
                                                            on lstMeasures.VersionKey equals lstVersions.VersionKey
                                                        join lstIppsnaps in ippSnapshots
                                                            on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                                        join lstProviders in hospitalDefaults
                                                            on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                                        where exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                        lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                        lstMeasures.MeasureSetID == selectedMeasureset &&
                                                        lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                                                        lstProviders.HospitalDefaultName == "ProviderID" &&
                                                        lstIppsnaps.HospitalID==Providerslst.HospitalID &&
                                                        ((measures.Contains(lstIppsnaps.AccountNumber)) && (ippkeys.Contains(lstIppsnaps.IPPIdentity.ToString()))) &&
                                                        ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null && exportParameters.SelectedHospID == null)) 
                                                        select new {
                                                            IppKey = lstPatients.IPPKey,
                                                            Adate = lstIppsnaps.AdmitDateTime,
                                                            Ddate = lstIppsnaps.DischargeDateTime,
                                                            UnitNo = lstIppsnaps.UnitNumber,
                                                            AccNo = lstIppsnaps.AccountNumber,
                                                            MeasureID = lstMeasures.MeasureSetID,
                                                            Casedata = from lstCDatas in casedatas
                                                                       where
                                                                           lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                                                                           !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)
                                                                       select new
                                                                       {
                                                                           AnswerCode = lstCDatas.DataValue,
                                                                           Questioncode = lstCDatas.DataKey
                                                                       }
                                                        },                                                                                                                                      
                                          };
                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return null;
                    }
                    else
                    {
                        int count = patientdatalist.Count();
                        //This outer loop displays Patient List
                        foreach (var providerlst in patientdatalist)
                        {
                            int Patientcount = providerlst.patients.ToList().Count();              
                            Provider provider = new Provider();
                            provider.Providerid = providerlst.providerID;
                            provider.Patient = new List<Patient>();
                            foreach (var patientlst in providerlst.patients)
                            {
                                
                                //Object creation of serialized classes
                                Patient allpatients = new Patient();
                            allpatients.Episodeofcare = new Episodeofcare();
                            Episodeofcare episodeOfCare = new Episodeofcare();
                            episodeOfCare.Detail = new List<Detail>();

                            //List of QuestionCodes for generating RowCount
                            List<string> lstQuestioncd = new List<string>();

                            //Code for Mapping database fields with XML serialized classes
                            episodeOfCare.Measureset = patientlst.MeasureID;
                            episodeOfCare.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                            episodeOfCare.Dischargedate = ((Convert.ToDateTime(patientlst.Ddate)).Date).ToString("MM-dd-yyyy");
                            episodeOfCare.Hospitalpatientid = patientlst.UnitNo;
                            episodeOfCare.Medicalrecordnumber = patientlst.AccNo;
                            episodeOfCare.Pthic = "Test001";
                                if (exportParameters.SelectedAction == "ADD")
                                {
                                    //This inner loop displays Details(Question_cd,Answer_cd) of respective Patient from outer loop
                                    foreach (var casedatalst in patientlst.Casedata)
                                    {
                                        List<string> answerList = new List<string>();
                                        RowNumberGenerator obj = new RowNumberGenerator();

                                        int chkcount = 0;
                                        //RowNumber Logic
                                        answerList = obj.GetRowNo(casedatalst.Questioncode, casedatalst.AnswerCode);
                                        foreach (var answer in answerList)
                                        {
                                            Detail detail = new Detail();
                                            detail.Questioncd = casedatalst.Questioncode;
                                            detail.Answercode = answer;
                                            detail.Rownumber = chkcount.ToString();
                                            chkcount++;
                                            episodeOfCare.Detail.Add(detail);
                                        }
                                    }
                                }
                            allpatients.Episodeofcare = episodeOfCare;                            
                            provider.Patient.Add(allpatients);
                            
                        }
                            if (Patientcount != 0)
                            {
                                submission.Provider.Add(provider);
                            }
                          
                        }
                            
                    }
                }
                _log.Info("Exit for DataFetch Method of DataMapper");
                return submission;
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while executing DataFetch Method of DataMapper" + ex.Message);
                return null;
            }
        }
    }
}
    
