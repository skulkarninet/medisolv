﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Xml.Serialization;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;


namespace ExportPlugins.IPPSExport
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'Perinatal Care – Newborns with BSI (PC-BSI)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-PC_BSI")]
    public class PCBSIIPPSExport : IExportService
    {
          Logger _log;
          public PCBSIIPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        DataMapper.IPPSDataMapper dataMapping = new DataMapper.IPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of PC_BSI MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
            try
            {
                _log.Info("Entered Export XML for PCBSIIPPS");
                //Object of Submission class to store result of 'DataFetch' method 
                Submission submission = new Submission();

                //Call to 'DataFetch' Method 
                //submission = dataMapping.DataFetch(exportParameters, Constants.measuresettest.PC_BSI.ToString());
                submission.Provider = new List<Provider>();
                var measures = exportParameters.PatientList.Where(m => m.Selected == true).Select(m => m.AccountNumber).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "PC_BSI")).Select(m => m.IPPKey).ToList();

                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {
                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //Constant values for unavailable mapping fields of Submission Class
                    submission.Type = Constants.SubmissionType;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)                   
                    var patientdatalist = from Providerslst in hospitalDefaults
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID"
                                          select new
                                          {
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              patients = from lstPatients in initialPatientPopulations
                                                         join lstMeasures in measureSets
                                                             on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                                         join lstVersions in versions
                                                             on lstMeasures.VersionKey equals lstVersions.VersionKey
                                                         join lstIppsnaps in ippSnapshots
                                                             on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                                         join lstProviders in hospitalDefaults
                                                             on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                                         orderby lstIppsnaps.AccountNumber
                                                         where
                                                             exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                             lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                             lstMeasures.MeasureSetID == "PC" &&
                                                             lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                                                             lstProviders.HospitalDefaultName == "ProviderID" &&
                                                             lstIppsnaps.HospitalID == Providerslst.HospitalID &&
                                                             ((measures.Contains(lstIppsnaps.AccountNumber)) && (ippkeys.Contains(lstIppsnaps.IPPIdentity.ToString()))) &&
                                                             ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null))
                                                         select new
                                                         {
                                                             IppKey = lstPatients.IPPKey,
                                                             Adate = lstIppsnaps.AdmitDateTime,
                                                             Ddate = lstIppsnaps.DischargeDateTime,
                                                             UnitNo = lstIppsnaps.UnitNumber,
                                                             AccNo = lstIppsnaps.AccountNumber,
                                                             MeasureID = lstMeasures.MeasureSetID,
                                                             providerID = lstProviders.HospitalDefaultsValue,
                                                             Casedata = from lstCDatas in casedatas
                                                                        where
                                                                            lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                                                                           (lstCDatas.DataKey == "BIRTHWEIGHT" || lstCDatas.DataKey == "BLDINFCFMD" || lstCDatas.DataKey == "BLDINFPRSADM" || lstCDatas.DataKey == "CLNCLTRIAL" || lstCDatas.DataKey == "HOSPADMIN" || lstCDatas.DataKey == "PHYSICIAN_1" || lstCDatas.DataKey == "PHYSICIAN_2") &&
                                                                            !lstCDatas.DataKey.Contains(Constants.QuestionCodeMs)
                                                                        select new
                                                                        {
                                                                            AnswerCode = lstCDatas.DataValue,
                                                                            Questioncode = lstCDatas.DataKey
                                                                        }
                                                         }
                                          };
                       


                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return null;
                    }
                    else
                    {
                        //This outer loop displays Patient List
                        foreach (var providerlst in patientdatalist)
                        {
                            int Patientcount = providerlst.patients.ToList().Count();
                            Provider provider = new Provider();
                            provider.Providerid = providerlst.providerID;
                            //Creation of List of Patients specific to Provider
                            provider.Patient = new List<Patient>();
                            foreach (var patientlst in providerlst.patients)
                            {
                                //Object creation of serialized classes
                                Patient allpatients = new Patient();
                                allpatients.Episodeofcare = new Episodeofcare();
                                Episodeofcare episodeOfCare = new Episodeofcare();
                                episodeOfCare.Detail = new List<Detail>();

                                //List of QuestionCodes for generating RowCount
                                List<string> lstQuestioncd = new List<string>();

                                //Code for Mapping database fields with XML serialized classes
                                episodeOfCare.Measureset = "PC_BSI";
                                episodeOfCare.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                                episodeOfCare.Dischargedate = ((Convert.ToDateTime(patientlst.Ddate)).Date).ToString("MM-dd-yyyy");
                                episodeOfCare.Hospitalpatientid = patientlst.UnitNo;
                                episodeOfCare.Medicalrecordnumber = patientlst.AccNo;

                                if (exportParameters.SelectedAction == "ADD")
                                {
                                    //This inner loop displays Details(Question_cd,Answer_cd) of respective Patient from outer loop
                                    foreach (var casedatalst in patientlst.Casedata)
                                    {
                                        List<string> ans = new List<string>();
                                        RowNumberGenerator obj = new RowNumberGenerator();

                                        //RowNumber Logic
                                        ans = obj.GetRowNo(casedatalst.Questioncode, casedatalst.AnswerCode);
                                        int chkcount = 0;
                                        foreach (var check in ans)
                                        {
                                            Detail detail = new Detail();
                                            detail.Questioncd = casedatalst.Questioncode;
                                            detail.Answercode = check;
                                            detail.Rownumber = chkcount.ToString();
                                            episodeOfCare.Detail.Add(detail);
                                            chkcount++;
                                        }
                                    }
                                }
                                allpatients.Episodeofcare = episodeOfCare;
                                provider.Patient.Add(allpatients);                               
                            }
                            if (Patientcount != 0)
                            {
                                submission.Provider.Add(provider);
                            }
                        }
                    }
                }

                //If Patient data is not available 
                if (submission == null)
                    return new ExportResult()
                    {
                        IsValid = false,
                        ErrorMessage = "There are no patients available whithin these dates"
                    };
                //If Patient data is available
                else
                {
                    XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                    XmlAttributes attrs = new XmlAttributes();

                    /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                       applied to the Comment field. Thus it will be serialized.*/
                    attrs.XmlIgnore = true;
                    xOver.Add(typeof(Episodeofcare), "pthic", attrs);
                    //Call to 'GenerateXml' Method for Serialization
                    XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                    bool exists = System.IO.Directory.Exists(XmlFilePath);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(XmlFilePath);
                    XmlFilePath = "PCBSIIPPS";
                    XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters);
                    _log.Info("Exit for Export method of PCBSI IPPS Measure");
                    return new ExportResult() { IsValid = true }; 
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting PCBSI IPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
