﻿using System;
using System.ComponentModel.Composition;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.Model;
using ExportPlugins.DataMapping;

namespace ExportPlugins.IPPSExport
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'HEART FAILURE (HF)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-HF")]
    public class HFIPPSExport : IExportService
    {
        /// <summary>
        /// XML file storage path 'D:\\ExportXML.xml'
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        DataMapping.DataMapping dataMapping = new DataMapping.DataMapping();

        /// <summary>
        /// This IExportService interface Method generates Export of HF MeasureSet
        /// </summary>     
       public ExportResult ExportXML(ExportParameters exportParameters)
        {
            try
            {
                //Object of Submission class to store result of 'DataFetch' method 
                Submission submission = new Submission();

                //Call to 'DataFetch' Method 
                submission = dataMapping.DataFetch(exportParameters, Constants.HfMeasureset);

                //If Patient data is not available 
                if (submission == null)
                    return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                //If Patient data is available
                else
                {
                    //Call to 'GenerateXml' Method for Serialization
                    Serialization.GenerateXml(XmlFilePath, submission);
                    return new ExportResult() { IsValid = true };
                }
            }
            catch (Exception)
            {
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
       public ExportResult ExportCSV(ExportParameters exportParameters)
       {
           //Database access through entity framework
           return new ExportResult() { IsValid = true };
       }
    }
}
