﻿using System;
using System.ComponentModel.Composition;
using System.Xml.Serialization;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;


namespace ExportPlugins.IPPSExport
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'ACUTE MYOCARDIAL INFARCTION (AMI)'.
    /// </summary>
    
    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-AMI")]

    public class AMIIPPSExport : IExportService
    {
         Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="AMIIPPSExport"/> class.
        /// </summary>
        public AMIIPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }

        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        //private static string serverFilePath = System.Configuration.ConfigurationManager.AppSettings["ServerFilePath"];

        DataMapper.IPPSDataMapper dataMapping=new DataMapper.IPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of AMI MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {            
            try
            {
                _log.Info("Entered Export XML for AMIIPPS");
                //Object of Submission class to store result of 'DataFetch' method 
                Submission submission=new Submission();

                //Call to 'DataFetch' Method 
                submission=dataMapping.DataFetch(exportParameters,Constants.Enummeasureset.AMI.ToString(), "AMI");

                //If Patient data is not available 
                if(submission==null)
                    return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                //If Patient data is available
                else
                {
                    XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                    XmlAttributes attrs = new XmlAttributes();

                    /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                       applied to the Comment field. Thus it will be serialized.*/
                    attrs.XmlIgnore = true;
                    xOver.Add(typeof(Episodeofcare), "Pthic", attrs);


                    //Call to 'GenerateXml' Method for Serialization
                    XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                    bool exists = System.IO.Directory.Exists(XmlFilePath);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(XmlFilePath);
                    XmlFilePath = "AMIIPPS";
                    XMLSerializer.GenerateXml(XmlFilePath, submission, xOver,exportParameters);

                    _log.Info("Exit for Export method of AMI IPPS Measure");
                    return new ExportResult() { IsValid = true };     
                }                                                                                        
            }
            catch (Exception ex)
            {
                 _log.Error("Error occured while exporting AMI IPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = ex.ToString()};             
            }
        }

        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            return new ExportResult() { IsValid = true };
        }
    }
}
