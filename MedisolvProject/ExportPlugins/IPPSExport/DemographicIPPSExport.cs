﻿using System;
using System.ComponentModel.Composition;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using ExportPlugins.DataAccess;
using EncorExportWebUI.ViewModels;
using System.Xml.Serialization;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.IPPSExport
{
    /// <summary>
    /// This class Generates DEMOGRAPHIC XML file for all MeasureSets.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-DEMOGRAPHIC")]
    public class DemographicIPPSExport : IExportService
    {
         Logger _log;
        /// <summary>
        /// Initializes a new instance of the <see cref="DemographicIPPSExport"/> class.
        /// </summary>
        public DemographicIPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        public bool checkUnknownMeasureSet = false;
        public bool checkMultipleMeasureSet = false;
        DataMapper.IPPSDataMapper dataMapping = new DataMapper.IPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of All MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {           

            try
            {
                _log.Info("Entered Export XML for DEMOGRAPHICDATAIPPS");
                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();
                List<string> done = new List<string>();
                List<string> provID = new List<string>();
                List<string> nipID = new List<string>();

                List<string> lstSelectedIPPKey = new List<string>();
                List<string> lstSelectedMeasureSet = new List<string>();

                //Gets selected Patient Account number and MeasureSet from Grid
                var lstAccountNum = exportParameters.PatientList.Where(m => m.Selected == true).Select(m => m.AccountNumber).ToList();
                var lstmeasureset = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "DEMOGRAPHIC")).Select(m => m.MeasureSet).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "DEMOGRAPHIC")).Select(m => m.IPPKey).ToList();

                lstSelectedIPPKey = ippkeys;
                lstSelectedMeasureSet = lstmeasureset;
                List<KeyValuePair<string, string>> checkProvID = new List<KeyValuePair<string, string>>();
                //Saves the Selected patient Account number And MeasureSet in Key-Value pair list
                List<KeyValuePair<string, string>> Acc_MeasureSet_Collection = new List<KeyValuePair<string, string>>();
                for (int j = 0; j < lstSelectedIPPKey.Count; j++)
                {
                    Acc_MeasureSet_Collection.Add(new KeyValuePair<string, string>(lstSelectedIPPKey[j], lstSelectedMeasureSet[j]));
                }               

                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //Constant values for unavailable mapping fields of Submission Class
                    submission.Type = Constants.SubmissionType;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)
                    
                    var patientdatalist = from Providerslst in hospitalDefaults
                                          from lstNpi in hospitalDefaults
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID" &&
                                              lstNpi.HospitalDefaultName == "NationalProviderID"                                         
                                          select new
                                          {
                                              npiID = lstNpi.HospitalID == Providerslst.HospitalID? lstNpi.HospitalDefaultsValue:null,
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              //npiID = lstNpi.HospitalDefaultsValue,
                                              hospitalIDNpi=lstNpi.HospitalID,
                                              hospitalIDPro= Providerslst.HospitalID,
                                              patients = from lstPatients in initialPatientPopulations
                                                         join lstMeasures in measureSets
                                                             on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                                         join lstVersions in versions
                                                             on lstMeasures.VersionKey equals lstVersions.VersionKey
                                                         join lstIppsnaps in ippSnapshots
                                                             on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                                         join lstProviders in hospitalDefaults
                                                               on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                                         orderby lstIppsnaps.AccountNumber
                                                         where
                                                             exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                             lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                             lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                                                              lstProviders.HospitalDefaultName == "ProviderID" &&
                                                              lstIppsnaps.HospitalID == Providerslst.HospitalID &&
                                                              // lstIppsnaps.HospitalID == lstNpi.HospitalID &&
                                                             (lstAccountNum.Contains(lstIppsnaps.AccountNumber) && lstmeasureset.Contains(lstMeasures.MeasureSetID) && (ippkeys.Contains(lstIppsnaps.IPPIdentity.ToString()))) &&
                                                              ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null && exportParameters.SelectedHospID == null))
                                                        //  ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null && exportParameters.SelectedHospID == null))
                                                         select new
                                                         {
                                                             IppKey = lstPatients.IPPKey,
                                                             hospiId=lstIppsnaps.HospitalID,
                                                             Adate = lstIppsnaps.AdmitDateTime,
                                                             Ddate = lstIppsnaps.DischargeDateTime,
                                                             UnitNo = lstIppsnaps.UnitNumber,
                                                             AccNo = lstIppsnaps.AccountNumber,
                                                             MeasureID = lstMeasures.MeasureSetID,
                                                             providerID = lstProviders.HospitalDefaultsValue,
                                                             npiID = lstNpi.HospitalDefaultsValue,
                                                             providerName = lstProviders.HospitalDefaultName,
                                                             Casedata = from lstCDatas in casedatas
                                                                        where
                                                                            lstCDatas.IPPIdentity == lstPatients.IPPKey
                                                                        select new
                                                                        {
                                                                            AnswerCode = lstCDatas.DataValue,
                                                                            Questioncode = lstCDatas.DataKey
                                                                        }
                                                         }
                                          };

                                  

                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                    }
                    else
                    {
                        foreach (var providerlstCheck in patientdatalist)
                        {
                            provID.Add(providerlstCheck.providerID);
                            nipID.Add(providerlstCheck.npiID);
                        }
                        for (int j = 0; j < provID.Count; j++)
                        {
                            checkProvID.Add(new KeyValuePair<string, string>(provID[j], nipID[j]));
                        }
                        int count = patientdatalist.Count();
                        //This outer loop displays Patient List
                        foreach (var providerlst in patientdatalist)
                        {
                           // string test2;
                            var checkNpi = checkProvID.Where(m => (m.Key==providerlst.providerID) && (m.Value != null)).Select(m => m.Value).FirstOrDefault();
                            if (providerlst.npiID==null && checkNpi!= null)
                            {
                                continue;
                            }
                            if (done.Contains(providerlst.providerID))
                            {
                                continue;
                            }
                            int Patientcount = providerlst.patients.ToList().Count();
                                Provider provider = new Provider();
                                provider.Providerid = providerlst.providerID;
                                if (providerlst.hospitalIDNpi == providerlst.hospitalIDPro)
                                {
                                    provider.npi = providerlst.npiID;
                                }

                                provider.Patient = new List<Patient>();
                                foreach (var patientlst in providerlst.patients)
                                {

                                    // provider.npi = patientlst.npiID;
                                    checkUnknownMeasureSet = false;
                                    checkMultipleMeasureSet = false;

                                    var MeasureSetValue = Acc_MeasureSet_Collection.Where(m => m.Key == patientlst.IppKey.ToString()).Select(m => m.Value).FirstOrDefault();
                                    if (patientlst.MeasureID != MeasureSetValue)
                                    {
                                        checkMultipleMeasureSet = true;
                                    }

                                    if (checkMultipleMeasureSet == true)
                                        continue;

                                    //Object creation of serialized classes
                                    Patient allpatients = new Patient();
                                    allpatients.Episodeofcare = new Episodeofcare();
                                    Episodeofcare episodeOfCare = new Episodeofcare();
                                    episodeOfCare.Detail = new List<Detail>();

                                    //Code for Mapping database fields with XML serialized classes
                                    episodeOfCare.Measureset = patientlst.MeasureID;
                                    episodeOfCare.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                                    episodeOfCare.Dischargedate = ((Convert.ToDateTime(patientlst.Ddate)).Date).ToString("MM-dd-yyyy");
                                    episodeOfCare.Hospitalpatientid = patientlst.UnitNo;
                                    episodeOfCare.Medicalrecordnumber = patientlst.AccNo;
                                    GlobalSets ed = new GlobalSets();
                                    if (exportParameters.SelectedAction == "ADD")
                                    {
                                        //This inner loop displays Details(Question_cd,Answer_cd) of respective Patient from outer loop
                                        foreach (var casedatalst in patientlst.Casedata)
                                        {
                                            //Gets HIC Number
                                            if (casedatalst.Questioncode == "MS_Payment" && casedatalst.AnswerCode.StartsWith("Medicare~"))
                                            {
                                                episodeOfCare.Pthic = casedatalst.AnswerCode.Substring(casedatalst.AnswerCode.LastIndexOf('~') + 1);
                                            }

                                            //Gets question_code and Answer_Code for Detail tag Excluding Question_Code with 'MS_'                                
                                            else if (!casedatalst.Questioncode.Contains("MS_"))
                                            {
                                                List<string> ans = new List<string>();
                                                RowNumberGenerator obj = new RowNumberGenerator();

                                                //RowNumber Logic
                                                ans = obj.GetRowNo(casedatalst.Questioncode, casedatalst.AnswerCode);
                                                int chkcount = 0;
                                                foreach (var check in ans)
                                                {
                                                    Detail detail = new Detail();
                                                    detail.Questioncd = casedatalst.Questioncode;
                                                    detail.Answercode = check;
                                                    detail.Rownumber = chkcount.ToString();
                                                    episodeOfCare.Detail.Add(detail);
                                                    chkcount++;
                                                }

                                                //Checks Sub MeasureSet of Global
                                                if (patientlst.MeasureID == "GLOBAL" || patientlst.MeasureID == "PC" ||
                                                    patientlst.MeasureID == "HBIPS-GLOBAL")
                                                {
                                                    episodeOfCare.Measureset = ed.GlobalDataFetch(casedatalst.Questioncode,
                                                        patientlst.MeasureID, checkUnknownMeasureSet);
                                                    if (episodeOfCare.Measureset != null)
                                                    {
                                                        checkUnknownMeasureSet = true;
                                                    }
                                                }
                                                else
                                                {
                                                    episodeOfCare.Measureset = patientlst.MeasureID;
                                                    checkUnknownMeasureSet = true;
                                                }
                                            }
                                            else
                                            {
                                                switch (casedatalst.Questioncode)
                                                {
                                                    case "MS_BirthDate":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.birthdate = ((Convert.ToDateTime(casedatalst.AnswerCode)).Date).ToString("MM-dd-yyyy");
                                                        break;
                                                    case "MS_Sex":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.sex = casedatalst.AnswerCode;
                                                        break;
                                                    case "MS_Race":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.race = casedatalst.AnswerCode;
                                                        break;
                                                    case "MS_HispanicEthnicity":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.ethnic = casedatalst.AnswerCode;
                                                        break;
                                                    case "MS_ZipCode":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.postalcode = casedatalst.AnswerCode;
                                                        break;
                                                    case "MS_PatientFirstName":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.firstname = casedatalst.AnswerCode;
                                                        break;
                                                    case "MS_PatientLastName":
                                                        if (casedatalst.AnswerCode != "")
                                                            allpatients.lastname = casedatalst.AnswerCode;
                                                        break;

                                                }
                                            }

                                        }
                                    }
                                    //Checks for Unknown MeasureSet.
                                    if (episodeOfCare.Measureset == null && checkUnknownMeasureSet == false &&
                                        (patientlst.MeasureID == "GLOBAL" || patientlst.MeasureID == "PC" ||
                                         patientlst.MeasureID == "HBIPS-GLOBAL"))
                                    {
                                        episodeOfCare.Measureset = patientlst.MeasureID;
                                        checkUnknownMeasureSet = true;
                                    }
                                    else if (episodeOfCare.Measureset == null)
                                    {
                                        episodeOfCare.Measureset = patientlst.MeasureID;
                                        checkUnknownMeasureSet = true;
                                    }
                                    if (exportParameters.SelectedAction == "ADD")
                                    {
                                        //Hardcoding for Unavailable Mapping fiels.
                                        allpatients.attesting_physician_code = "APC001";
                                        allpatients.attesting_physician_specialty_code = "APSC001";
                                        allpatients.principal_procedure_physician_code = "PPPC001";
                                        allpatients.psychiatric_flag = "Flag001";
                                    }
                                    allpatients.Episodeofcare = episodeOfCare;
                                    provider.Patient.Add(allpatients);
                                }
                                if (Patientcount != 0)
                                {
                                    submission.Provider.Add(provider);
                                done.Add(providerlst.providerID);
                            }
                            }
                        

                        XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                        XmlAttributes attrs = new XmlAttributes();

                        /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                           applied to the Comment field. Thus it will be serialized.*/
                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Episodeofcare), "Pthic", attrs);

                        attrs.XmlIgnore = false;
                        
                        xOver.Add(typeof(Patient), "birthdate", attrs);              

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "sex", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "race", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "ethnic", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "attesting_physician_code", attrs);                       

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "attesting_physician_specialty_code", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "principal_procedure_physician_code", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "psychiatric_flag", attrs);

                        //Call to 'GenerateXml' Method for Serialization
                        XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                        bool exists = System.IO.Directory.Exists(XmlFilePath);

                        if (!exists)
                            System.IO.Directory.CreateDirectory(XmlFilePath);
                        XmlFilePath ="DemographicDataIPPS";
                        XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters); 
                    }
                }
                _log.Info("Exit for Export method of SEP IPPS Measure");
                return new ExportResult() { IsValid = true };   
            }
            catch (Exception ex)
            {
                 _log.Error("Error occured while exporting SEP IPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
