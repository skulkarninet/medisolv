﻿using System;
using System.ComponentModel.Composition;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using ExportPlugins.DataAccess;
using System.Xml.Serialization;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.IPPSExport
{
    /// <summary>
    /// This class Generates XML file for 'HBIPS_EVT' MeasureSets.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-HBIPS_EVT")]
    class HBIPS_EVTIPPSExport : IExportService
    {
        Logger _log;
        public HBIPS_EVTIPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        DataMapper.IPPSDataMapper dataMapping = new DataMapper.IPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of HBIPS_EVT MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
            try
            {
                _log.Info("Entered Export XML for HBIPS_EVT");
                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();
                var measures = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "HBIPS_EVT")).Select(m => m.AccountNumber).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "HBIPS_EVT")).Select(m => m.IPPKey).ToList();

                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<EventPatientPopulation> eventPatientPopulations = context.EventPatientPopulations;
                    DbSet<EventData> eventData = context.EventDatas;
                    DbSet<EventMeasureStatu> eventMeasureventMeasureStatu = context.EventMeasureStatus;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //Constant values for unavailable mapping fields of Submission Class
                    submission.Type = Constants.SubmissionType;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)
                   
                    var patientdatalist = from Providerslst in hospitalDefaults
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID"
                                          select new
                                          {
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              patients = from lstVersions in versions
                                                         join lstMeasures in measureSets
                                                         on lstVersions.VersionKey equals lstMeasures.VersionKey
                                                         join lstPatients in eventPatientPopulations
                                                         on lstMeasures.MeasureSetKey equals lstPatients.MeasureSetKey
                                                         join lstProviders in hospitalDefaults
                                                         on lstPatients.HospitalID equals lstProviders.HospitalID
                                                         orderby lstPatients.AccountNumber
                                                         where
                                                             exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                             lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                             lstMeasures.MeasureSetID == "HBIPS-EVT" &&
                                                             lstProviders.HospitalDefaultName == "ProviderID" &&
                                                             lstPatients.HospitalID == Providerslst.HospitalID &&
                                                             ((measures.Contains(lstPatients.AccountNumber)) && (ippkeys.Contains(lstPatients.EventKey.ToString()))) &&
                                                          ((lstPatients.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstPatients.HospitalID.ToString() != null && exportParameters.SelectedHospID == null))
                                                         select new
                                                         {
                                                             IppKey = lstPatients.EventKey,
                                                             Adate = lstPatients.AdmitDateTime,
                                                             Ddate = lstPatients.DischargeDateTime,
                                                             UnitNo = lstPatients.UnitNumber,
                                                             AccNo = lstPatients.AccountNumber,
                                                             MeasureID = lstMeasures.MeasureSetID,
                                                             providerID = lstProviders.HospitalDefaultsValue,
                                                             Casedata = from lstCDatas in eventData
                                                                        where lstCDatas.EventKey == lstPatients.EventKey
                                                                        select new
                                                                        {
                                                                            AnswerCode = lstCDatas.DataValue,
                                                                            Questioncode = lstCDatas.DataKey
                                                                        }
                                                         }
                                          };


                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                    }
                    else
                    {
                        //This outer loop displays Patient List
                        foreach (var providerlst in patientdatalist)
                        {
                            int Patientcount = providerlst.patients.ToList().Count();
                            Provider provider = new Provider();
                            provider.Providerid = providerlst.providerID;
                            //Creation of List of Patients specific to Provider
                            provider.Patient = new List<Patient>();
                            foreach (var patientlst in providerlst.patients)
                            {

                                //Object creation of serialized classes
                                Patient allpatients = new Patient();
                                allpatients.Episodeofcare = new Episodeofcare();
                                Episodeofcare episodeofcare = new Episodeofcare();
                                episodeofcare.Detail = new List<Detail>();

                                //Code for Mapping database fields with XML serialized classes
                                episodeofcare.Measureset = patientlst.MeasureID;
                                episodeofcare.Admitdate = ((Convert.ToDateTime(patientlst.Adate)).Date).ToString("MM-dd-yyyy");
                                episodeofcare.Hospitalpatientid = patientlst.UnitNo;
                                episodeofcare.Medicalrecordnumber = patientlst.AccNo;
                                if (exportParameters.SelectedAction == "ADD")
                                {
                                    //This inner loop displays Details(Question_cd,Answer_cd) of respective Patient from outer loop
                                    foreach (var casedatalst in patientlst.Casedata)
                                    {
                                        if (casedatalst.Questioncode == "MS_Payment" &&
                                            casedatalst.AnswerCode.StartsWith("Medicare~"))
                                        {
                                            episodeofcare.Pthic =
                                                casedatalst.AnswerCode.Substring(casedatalst.AnswerCode.LastIndexOf('~') + 1);
                                        }
                                        else if (!casedatalst.Questioncode.Contains(Constants.QuestionCodeMs))
                                        {
                                            List<string> ans = new List<string>();
                                            RowNumberGenerator obj = new RowNumberGenerator();
                                            ans = obj.GetRowNo(casedatalst.Questioncode, casedatalst.AnswerCode);
                                            int chkcount = 0;
                                            foreach (var check in ans)
                                            {
                                                Detail detail = new Detail();
                                                detail.Questioncd = casedatalst.Questioncode;
                                                detail.Answercode = check;
                                                detail.Rownumber = chkcount.ToString();
                                                episodeofcare.Detail.Add(detail);
                                                chkcount++;
                                            }
                                        }
                                        else
                                        {
                                            switch (casedatalst.Questioncode)
                                            {
                                                case "MS_BirthDate":
                                                    if (casedatalst.AnswerCode != "")
                                                        allpatients.birthdate = ((Convert.ToDateTime(casedatalst.AnswerCode)).Date).ToString("MM-dd-yyyy");
                                                    break;
                                                case "MS_Sex":
                                                    if (casedatalst.AnswerCode != "")
                                                        allpatients.sex = casedatalst.AnswerCode;
                                                    break;
                                                case "MS_Race":
                                                    if (casedatalst.AnswerCode != "")
                                                        allpatients.race = casedatalst.AnswerCode;
                                                    break;
                                                case "MS_HispanicEthnicity":
                                                    if (casedatalst.AnswerCode != "")
                                                        allpatients.ethnic = casedatalst.AnswerCode;
                                                    break;
                                                case "MS_ZipCode":
                                                    if (casedatalst.AnswerCode != "")
                                                        allpatients.postalcode = casedatalst.AnswerCode;
                                                    break;

                                            }
                                        }
                                    }
                                }
                                if (exportParameters.SelectedAction == "ADD")
                                {
                                    allpatients.attesting_physician_code = "APC001";
                                    allpatients.attesting_physician_specialty_code = "APSC001";
                                }
                                allpatients.Episodeofcare = episodeofcare;
                                provider.Patient.Add(allpatients);
                            }
                            if (Patientcount != 0)
                            {
                                submission.Provider.Add(provider);
                            }
                        }
                        XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                        XmlAttributes attrs = new XmlAttributes();

                        /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                           applied to the Comment field. Thus it will be serialized.*/
                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Episodeofcare), "Pthic", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "birthdate", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "sex", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "race", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "ethnic", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "attesting_physician_code", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "npi", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "attesting_physician_specialty_code", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "principal_procedure_physician_code", attrs);

                        attrs.XmlIgnore = false;
                        xOver.Add(typeof(Patient), "psychiatric_flag", attrs);

                        //Call to 'GenerateXml' Method for Serialization
                        XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                        bool exists = System.IO.Directory.Exists(XmlFilePath);

                        if (!exists)
                            System.IO.Directory.CreateDirectory(XmlFilePath);
                        XmlFilePath = "HBIPS_EVTIPPS";
                        XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters);
                    }
                }
                _log.Info("Exit for Export method of HBIPS_EVT IPPS Measure");
                return new ExportResult() { IsValid = true };
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting HBIPS_EVT IPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
