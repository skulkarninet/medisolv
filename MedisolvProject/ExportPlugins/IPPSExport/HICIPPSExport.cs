﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.DataAccess;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.IPPSExport
{

    /// <summary>
    /// This class Generates XML file for MeasureSet 'ACUTE MYOCARDIAL INFARCTION (AMI)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-HIC")]
    public class HICIPPSExport : IExportService
    {
        Logger _log;
        public HICIPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }
        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        public bool checkUnknownMeasureSet = false;
        public bool checkMultipleMeasureSet = false;  
        public bool checkPthic = false;

        /// <summary>
        /// This IExportService interface Method generates Export of SCIP MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
            try
            {
                _log.Info("Entered Export XML for HICIPPS");
                //Code for Object creation of serialized classes
                Submission submission = new Submission();
                submission.Provider = new List<Provider>();

                List<string> lstSelectedIPPKey = new List<string>();
                List<string> lstSelectedMeasureSet = new List<string>();

                //Gets selected Patient Account number and MeasureSet from Grid
                var lstAccountNum = exportParameters.PatientList.Where(m => m.Selected == true).Select(m => m.AccountNumber).ToList();
                var lstmeasureset = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "HIC")).Select(m => m.MeasureSet).ToList();
                var ippkeys = exportParameters.PatientList.Where(m => (m.Selected == true) && (m.Export == "HIC")).Select(m => m.IPPKey).ToList();

                lstSelectedIPPKey = ippkeys;
                lstSelectedMeasureSet = lstmeasureset;

                //Saves the Selected patient Account number And MeasureSet in Key-Value pair list
                List<KeyValuePair<string, string>> Acc_MeasureSet_Collection = new List<KeyValuePair<string, string>>();
                for (int j = 0; j < lstSelectedIPPKey.Count; j++)
                {
                    Acc_MeasureSet_Collection.Add(new KeyValuePair<string, string>(lstSelectedIPPKey[j], lstSelectedMeasureSet[j]));
                }
                //Code for Database access through Entity Framework 4.0
                using (RapidEntities context = new RapidEntities())
                {

                    //Access database tables
                    DbSet<InitialPatientPopulation> initialPatientPopulations = context.InitialPatientPopulations;
                    DbSet<CaseData> casedatas = context.CaseDatas;
                    DbSet<MeasureSet> measureSets = context.MeasureSets;
                    DbSet<ExportPlugins.DataAccess.Version> versions = context.Versions;
                    DbSet<IPPSnapshot> ippSnapshots = context.IPPSnapshots;
                    DbSet<HospitalDefault> hospitalDefaults = context.HospitalDefaults;

                    //Constant values for unavailable mapping fields of Submission Class
                    //provider.Providerid = Constants.SubmissionProviderid;
                    submission.Type = Constants.SubmissionType;
                    submission.Data = Constants.SubmissionData;
                    submission.Version = Constants.SubmissionVersion;
                    submission.Actioncode = exportParameters.SelectedAction;

                    //This Query access Patient Data with its Details(Question_cd,Answer_cd)                   
                   
                    var patientdatalist = from Providerslst in hospitalDefaults
                                          where
                                              Providerslst.HospitalDefaultName == "ProviderID"
                                          select new
                                          {
                                              providerID = Providerslst.HospitalDefaultsValue,
                                              patients = from lstPatients in initialPatientPopulations
                                                         join lstMeasures in measureSets
                                                             on lstPatients.MeasureSetKey equals lstMeasures.MeasureSetKey
                                                         join lstVersions in versions
                                                             on lstMeasures.VersionKey equals lstVersions.VersionKey
                                                         join lstIppsnaps in ippSnapshots
                                                             on lstPatients.IPPKey equals lstIppsnaps.IPPIdentity
                                                         join lstProviders in hospitalDefaults
                                                             on lstIppsnaps.HospitalID equals lstProviders.HospitalID
                                                         orderby lstIppsnaps.AccountNumber
                                                         where
                                                             exportParameters.SelectedPeriod.StartDate <= lstVersions.EndDate &&
                                                             lstVersions.StartDate <= exportParameters.SelectedPeriod.EndDate &&
                                                             lstIppsnaps.InpatientOrOutpatient == Constants.Specification.I.ToString() &&
                                                             lstProviders.HospitalDefaultName == "ProviderID" &&
                                                             lstIppsnaps.HospitalID == Providerslst.HospitalID &&
                                                             (lstAccountNum.Contains(lstIppsnaps.AccountNumber) && lstmeasureset.Contains(lstMeasures.MeasureSetID) && (ippkeys.Contains(lstIppsnaps.IPPIdentity.ToString()))) &&
                                                          ((lstIppsnaps.HospitalID.ToString() == exportParameters.SelectedHospID && exportParameters.SelectedHospID != null) || (lstIppsnaps.HospitalID != null && exportParameters.SelectedHospID == null))
                                                         select new
                                                         {
                                                             IppKey = lstPatients.IPPKey,
                                                             Adate = lstIppsnaps.AdmitDateTime,
                                                             Ddate = lstIppsnaps.DischargeDateTime,
                                                             UnitNo = lstIppsnaps.UnitNumber,
                                                             AccNo = lstIppsnaps.AccountNumber,
                                                             MeasureID = lstMeasures.MeasureSetID,
                                                             providerID = lstProviders.HospitalDefaultsValue,
                                                             providerName = lstProviders.HospitalDefaultName,
                                                             Casedata = from lstCDatas in casedatas
                                                                        where
                                                                            lstCDatas.IPPIdentity == lstPatients.IPPKey &&
                                                                            ((lstCDatas.DataKey == "MS_Payment" && lstCDatas.DataValue.StartsWith("Medicare~") || (!lstCDatas.DataKey.Contains(Constants.QuestionCodeMs))))
                                                                        select new
                                                                        {
                                                                            AnswerCode = lstCDatas.DataValue,
                                                                            Questioncode = lstCDatas.DataKey
                                                                        }
                                                         }
                                          };

                    //Check if Patient Data is availble between StartDate and EndDate
                    if (!patientdatalist.Any())
                    {
                        return new ExportResult() { IsValid = false, ErrorMessage = "There are no patients available whithin these dates" };
                    }
                    else
                    {
                        int count = patientdatalist.Count();
                        //This outer loop displays Patient List
                foreach (var providerlst in patientdatalist)
                        {
                            int Patientcount = providerlst.patients.ToList().Count();
                            Provider provider = new Provider();
                            provider.Providerid = providerlst.providerID;
                            //Creation of List of Patients specific to Provider
                            provider.Patient = new List<Patient>();
                            foreach (var patientlst in providerlst.patients)
                            {
                                checkUnknownMeasureSet = false;
                                checkMultipleMeasureSet = false;

                                var MeasureSetValue = Acc_MeasureSet_Collection.Where(m => m.Key == patientlst.IppKey.ToString()).Select(m => m.Value).FirstOrDefault();
                                if (patientlst.MeasureID != MeasureSetValue)
                                {
                                    checkMultipleMeasureSet = true;
                                }

                                if (checkMultipleMeasureSet == true)
                                    continue;

                                checkPthic = false;

                                //Object creation of serialized classes
                                Patient allpatients = new Patient();
                                allpatients.Encounter = new Encounter();
                                Encounter encounter = new Encounter();
                                encounter.Detail = new List<Detail>();

                                //List of QuestionCodes for generating RowCount
                                List<string> lstQuestioncd = new List<string>();

                                //Code for Mapping database fields with XML serialized classes

                                //encounter.Measureset = patientlst.MeasureID;
                                encounter.Admitdate = ((Convert.ToDateTime(patientlst.Ddate)).Date).ToString("MM-dd-yyyy");
                                encounter.Hospitalpatientid = patientlst.UnitNo;
                                encounter.Medicalrecordnumber = patientlst.AccNo;
                                GlobalSets ed = new GlobalSets();

                                foreach (var casedatalst in patientlst.Casedata)
                                {
                                    //Gets HIC Number
                                    if (casedatalst.Questioncode == "MS_Payment" &&
                                        casedatalst.AnswerCode.StartsWith("Medicare~"))
                                    {
                                        encounter.pthic = casedatalst.AnswerCode.Substring(casedatalst.AnswerCode.LastIndexOf('~') + 1);
                                    }

                                    else if (!casedatalst.Questioncode.Contains("MS_"))
                                    {
                                        List<string> ans = new List<string>();
                                        RowNumberGenerator obj = new RowNumberGenerator();
                                        ans = obj.GetRowNo(casedatalst.Questioncode, casedatalst.AnswerCode);
                                       

                                        //Checks Sub MeasureSet of Global
                                        if (patientlst.MeasureID == "GLOBAL" || patientlst.MeasureID == "PC" ||
                                            patientlst.MeasureID == "HBIPS-GLOBAL")
                                        {
                                            encounter.Measureset = ed.GlobalDataFetch(casedatalst.Questioncode, patientlst.MeasureID, checkUnknownMeasureSet);
                                            if (encounter.Measureset != null)
                                            {
                                                checkUnknownMeasureSet = true;
                                            }
                                        }
                                        else
                                        {
                                            encounter.Measureset = patientlst.MeasureID;
                                            checkUnknownMeasureSet = true;
                                        }
                                    }
                                    if (encounter.Measureset == null && checkUnknownMeasureSet == false)
                                    {
                                        encounter.Measureset = patientlst.MeasureID;
                                        checkUnknownMeasureSet = true;
                                    }
                                }

                                //Checks for Unknown MeasureSet.
                                if (encounter.Measureset == null && checkUnknownMeasureSet == false &&
                                    (patientlst.MeasureID == "GLOBAL" || patientlst.MeasureID == "PC" ||
                                     patientlst.MeasureID == "HBIPS-GLOBAL"))
                                {
                                    encounter.Measureset = patientlst.MeasureID;
                                    checkUnknownMeasureSet = true;
                                }
                                else if (encounter.Measureset == null)
                                {
                                    encounter.Measureset = patientlst.MeasureID;
                                    checkUnknownMeasureSet = true;
                                }

                                if (encounter.pthic == null && checkPthic == false)
                                {
                                    checkPthic = true;
                                }
                                if (checkPthic)
                                    continue;

                                allpatients.Encounter = encounter;
                                provider.Patient.Add(allpatients);
                               
                            }
                            if (Patientcount != 0)
                            {
                                submission.Provider.Add(provider);
                            }
                        }
                        XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                        XmlAttributes attrs = new XmlAttributes();

                        /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                           applied to the Comment field. Thus it will be serialized.*/
                        
                        //Call to 'GenerateXml' Method for Serialization
                        XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                        bool exists = System.IO.Directory.Exists(XmlFilePath);

                        if (!exists)
                            System.IO.Directory.CreateDirectory(XmlFilePath);
                        XmlFilePath = "HICIPPS";
                        XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters); 
                    }
                }
                _log.Info("Exit for Export method of HIC IPPS Measure");
                return new ExportResult() { IsValid = true };   
            }
            catch (Exception ex)
            {
                 _log.Error("Error occured while exporting HIC IPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
