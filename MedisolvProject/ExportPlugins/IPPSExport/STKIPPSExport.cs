﻿using System;
using System.ComponentModel.Composition;
using System.Xml.Serialization;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using ExportPlugins.DataMapper;
using ExportPlugins.Model;
using TracerX;

namespace ExportPlugins.IPPSExport
{
    /// <summary>
    /// This class Generates XML file for MeasureSet 'STROKE (STK)'.
    /// </summary>

    /// <summary>
    /// MEF Export service
    /// </summary> 
    [Export(typeof(IExportService))]
    [ExportMetadata("Measure", "IPPS-STK")]
    public class STKIPPSExport: IExportService
    {
        Logger _log;
        public STKIPPSExport()
        {
            _log = Container.ResolveLoggerInstance();
        }

        /// <summary>
        /// XML file storage path
        /// </summary>   
        private static string XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];

        DataMapper.IPPSDataMapper dataMapping = new DataMapper.IPPSDataMapper();

        /// <summary>
        /// This IExportService interface Method generates Export of STK MeasureSet
        /// </summary>     
        public ExportResult ExportXML(ExportParameters exportParameters)
        {
            try
            {
                _log.Info("Entered Export XML for STKIPPS");
                //Object of Submission class to store result of 'DataFetch' method 
                Submission submission = new Submission();

                //Call to 'DataFetch' Method 
                submission = dataMapping.DataFetch(exportParameters, Constants.Enummeasureset.STK.ToString(), "STK");

                //If Patient data is not available 
                if (submission == null)
                    return new ExportResult()
                    {
                        IsValid = false,
                        ErrorMessage = "There are no patients available whithin these dates"
                    };
                //If Patient data is available
                else
                {
                    XmlAttributeOverrides xOver = new XmlAttributeOverrides();
                    XmlAttributes attrs = new XmlAttributes();

                    /* Setting XmlIgnore to false overrides the XmlIgnoreAttribute
                       applied to the Comment field. Thus it will be serialized.*/
                    attrs.XmlIgnore = true;
                    xOver.Add(typeof(Episodeofcare), "pthic", attrs);
                    //Call to 'GenerateXml' Method for Serialization
                    XmlFilePath = System.Configuration.ConfigurationManager.AppSettings["ExportPath"];
                    bool exists = System.IO.Directory.Exists(XmlFilePath);

                    if (!exists)
                        System.IO.Directory.CreateDirectory(XmlFilePath);
                    XmlFilePath ="STKIPPS";
                    XMLSerializer.GenerateXml(XmlFilePath, submission, xOver, exportParameters);
                    _log.Info("Exit for Export method of STK IPPS Measure");
                    return new ExportResult() { IsValid = true }; 
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error occured while exporting STK IPPS Measure" + ex.Message);
                return new ExportResult() { IsValid = false, ErrorMessage = "Error occured while exporting!" };
            }
        }
        public ExportResult ExportCSV(ExportParameters exportParameters)
        {
            //Database access through entity framework
            return new ExportResult() { IsValid = true };
        }
    }
}
