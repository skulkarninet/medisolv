﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Episodeofcare.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "episode-of-care")]
    public class Episodeofcare
    {
        /// <summary>
        /// Gets or sets the admitdate.
        /// </summary>
        /// <value>The admitdate.</value>
        [XmlElement(ElementName = "admit-date")]
        public string Admitdate { get; set; }
        /// <summary>
        /// Gets or sets the dischargedate.
        /// </summary>
        /// <value>The dischargedate.</value>
        [XmlElement(ElementName = "discharge-date")]
        public string Dischargedate { get; set; }
        /// <summary>
        /// Gets or sets the hospitalpatientid.
        /// </summary>
        /// <value>The hospitalpatientid.</value>
        [XmlElement(ElementName = "hospital-patient-id")]
        public string Hospitalpatientid { get; set; }
        /// <summary>
        /// Gets or sets the medicalrecordnumber.
        /// </summary>
        /// <value>The medicalrecordnumber.</value>
        [XmlElement(ElementName = "medical-record-number")]
        public string Medicalrecordnumber { get; set; }
        /// <summary>
        /// Gets or sets the pthic.
        /// </summary>
        /// <value>The pthic.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "pthic")]
        public string Pthic { get; set; }
        /// <summary>
        /// Gets or sets the detail.
        /// </summary>
        /// <value>The detail.</value>
        [XmlElement(ElementName = "detail")]
        public List<Detail> Detail { get; set; }
        /// <summary>
        /// Gets or sets the measureset.
        /// </summary>
        /// <value>The measureset.</value>
        [XmlAttribute(AttributeName = "measure-set")]
        public string Measureset { get; set; }
    }
}
