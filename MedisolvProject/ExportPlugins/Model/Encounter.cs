﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Encounter.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "encounter")]
    public class Encounter
    {
        /// <summary>
        /// Gets or sets the admitdate.
        /// </summary>
        /// <value>The admitdate.</value>
        [XmlElement(ElementName = "encounter-date")]
        public string Admitdate { get; set; }

        /// <summary>
        /// Gets or sets the arrival_time.
        /// </summary>
        /// <value>The arrival_time.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "arrival-time")]
        public string arrival_time { get; set; }

        //[XmlIgnoreAttribute]
        /// <summary>
        /// Gets or sets the pthic.
        /// </summary>
        /// <value>The pthic.</value>
        [XmlElement(ElementName = "pthic")]
        public string pthic { get; set; }
        /// <summary>
        /// Gets or sets the hospitalpatientid.
        /// </summary>
        /// <value>The hospitalpatientid.</value>
        [XmlElement(ElementName = "patient-id")]
        public string Hospitalpatientid { get; set; }
        /// <summary>
        /// Gets or sets the medicalrecordnumber.
        /// </summary>
        /// <value>The medicalrecordnumber.</value>
        [XmlElement(ElementName = "medical-record-number")]
        public string Medicalrecordnumber { get; set; }     
        [XmlAttribute(AttributeName = "measure-set")]
        public string Measureset { get; set; }
        //[XmlIgnoreAttribute]
        /// <summary>
        /// Gets or sets the detail.
        /// </summary>
        /// <value>The detail.</value>
        [XmlElement(ElementName = "detail")]             
        public List<Detail> Detail { get; set; }
    }
}
