﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Patient.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "patient")]
    public class Patient
    {

        /// <summary>
        /// Gets or sets the birthdate.
        /// </summary>
        /// <value>The birthdate.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "birthdate")]
        public string birthdate { get; set; }
        /// <summary>
        /// Gets or sets the sex.
        /// </summary>
        /// <value>The sex.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "sex")]
        public string sex { get; set; }
        /// <summary>
        /// Gets or sets the race.
        /// </summary>
        /// <value>The race.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "race")]
        public string race { get; set; }
        /// <summary>
        /// Gets or sets the ethnic.
        /// </summary>
        /// <value>The ethnic.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "ethnic")]
        public string ethnic { get; set; }
        /// <summary>
        /// Gets or sets the postalcode.
        /// </summary>
        /// <value>The postalcode.</value>
        //[XmlIgnoreAttribute]
        [XmlElement(ElementName = "postal-code")]
        public string postalcode { get; set; }
        /// <summary>
        /// Gets or sets the attesting_physician_code.
        /// </summary>
        /// <value>The attesting_physician_code.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "attesting_physician_code")]
        public string attesting_physician_code { get; set; }
        /// <summary>
        /// Gets or sets the attesting_physician_specialty_code.
        /// </summary>
        /// <value>The attesting_physician_specialty_code.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "attesting_physician_specialty_code")]
        public string attesting_physician_specialty_code { get; set; }
        /// <summary>
        /// Gets or sets the principal_procedure_physician_code.
        /// </summary>
        /// <value>The principal_procedure_physician_code.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "principal_procedure_physician_code")]
        public string principal_procedure_physician_code { get; set; }
        /// <summary>
        /// Gets or sets the psychiatric_flag.
        /// </summary>
        /// <value>The psychiatric_flag.</value>
        [XmlIgnoreAttribute]
        [XmlElement(ElementName = "psychiatric_flag")]
        public string psychiatric_flag { get; set; }
        /// <summary>
        /// Gets or sets the firstname.
        /// </summary>
        /// <value>The firstname.</value>
        [XmlElement(ElementName = "first-name")]
        public string firstname { get; set; }

        /// <summary>
        /// Gets or sets the lastname.
        /// </summary>
        /// <value>The lastname.</value>
        [XmlElement(ElementName = "last-name")]
        public string lastname { get; set; }

        /// <summary>
        /// Gets or sets the encounter.
        /// </summary>
        /// <value>The encounter.</value>
        [XmlElement(ElementName = "encounter")]
        public Encounter Encounter { get; set; }

        /// <summary>
        /// Gets or sets the episodeofcare.
        /// </summary>
        /// <value>The episodeofcare.</value>
        [XmlElement(ElementName = "episode-of-care")]
        public Episodeofcare Episodeofcare { get; set; }

    }
}
