﻿using System;
using System.Xml.Serialization;

namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Detail.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "detail")]
    public class Detail
    {
        /// <summary>
        /// Gets or sets the answercode.
        /// </summary>
        /// <value>The answercode.</value>
        [XmlAttribute(AttributeName = "answer-code")]
        public string Answercode { get; set; }
        /// <summary>
        /// Gets or sets the rownumber.
        /// </summary>
        /// <value>The rownumber.</value>
        [XmlAttribute(AttributeName = "row-number")]
        public string Rownumber { get; set; }
        /// <summary>
        /// Gets or sets the questioncd.
        /// </summary>
        /// <value>The questioncd.</value>
        [XmlAttribute(AttributeName = "question-cd")]
        public string Questioncd { get; set; }
            
    }
}
