﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Submission.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "submission")]
    public class Submission
    {
        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        /// <value>The provider.</value>
        [XmlElement(ElementName = "provider")]
        public List<Provider> Provider { get; set; }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>The type.</value>
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        [XmlAttribute(AttributeName = "data")]
        public string Data { get; set; }
        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
        /// <summary>
        /// Gets or sets the actioncode.
        /// </summary>
        /// <value>The actioncode.</value>
        [XmlAttribute(AttributeName = "action-code")]
        public string Actioncode { get; set; }
                    
    }
}
