﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Provider.
    /// </summary>
    [Serializable]
    [XmlRoot(ElementName = "provider")]
    public class Provider
    {
        /// <summary>
        /// Gets or sets the providerid.
        /// </summary>
        /// <value>The providerid.</value>
        [XmlElement(ElementName = "provider-id")]
        public string Providerid { get; set; }
        /// <summary>
        /// Gets or sets the npi.
        /// </summary>
        /// <value>The npi.</value>
        [XmlElement(ElementName = "npi")]
        public string npi { get; set; }

        /// <summary>
        /// Gets or sets the patient.
        /// </summary>
        /// <value>The patient.</value>
        [XmlElement(ElementName = "patient")]
        public List<Patient> Patient { get; set; }
       
    }
}
