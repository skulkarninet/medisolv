﻿using System;
using System.Collections.Generic;
using System.IO;
using TracerX;


namespace ExportPlugins.Model
{
    /// <summary>
    /// Class Container for Creating log.
    /// </summary>
    public class Container
    {
         private static Dictionary<object, object> Context = new Dictionary<object, object>();

       private static object ResolveInstance(Type type)
       {
           object output = null;
           Context.TryGetValue(type, out output);
           return output;
       }

        /// <summary>
        /// Resolves the logger instance.
        /// </summary>
        /// <returns>Logger.</returns>
        public static Logger ResolveLoggerInstance()
       {
           if (!Context.ContainsKey(typeof(Logger)))
           {
               var path = System.AppDomain.CurrentDomain.BaseDirectory;
               path = string.Format(@"{0}{1}", path, "TracerX.xml");
               Logger.Xml.Configure(path);
               Logger.DefaultBinaryFile.Directory = LogFolder;
               var log = Logger.GetLogger("IderaLogs");
               Context.Add(typeof(Logger), log);
               Logger.DefaultBinaryFile.Open();
           }
            return  Container.ResolveInstance(typeof(Logger)) as Logger;
       }
       private static string LogFolder
       {
           get
           {
               return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData, Environment.SpecialFolderOption.Create), LogsFolder);
           }
       }
       private const string LogsFolder = "Encor\\Exports\\Logs";
    }
}
