/*
	Dropdown with Multiple checkbox select with jQuery - May 27, 2013
	(c) 2013 @ElmahdiMahmoud
	license: http://www.opensource.org/licenses/mit-license.php
*/
var lst = [];
var count = 0;
var lstLength = 0;
var selectedCount = 0;
var visited = 0;

$(".dropdown dt a").on('click', function () {
    $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function () {
    $(".dropdown dd ul").hide();
});

function getSelectedValue(id) {
    return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function (e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function () {
    // debugger;
    var text;
    var check = 'post';
    //var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
    //  title = $(this).val() + ",";
    var res = $('.multiSel').val();
    if ($(this).is(':checked')) {
        var selectedValue = $(this)[0].id.substring(5, $(this)[0].id.length);
        if ($('.multiSel')[0].innerText == "")
            text = selectedValue;
        else
            text = "," + selectedValue;
        selectedCount++;
        var id = text.replace(',', '');
        var html = '<span id="' + id + '">' + text + '</span>';
        if (selectedCount > 3) {
            $('.multiSel').empty();
            var htmlTest = '<span  id=more>' + 'More than 3 are selected' + '</span>'
            $('.multiSel').append(htmlTest);
            visited = 1;
            if (lst.indexOf(html) > 0)
            { }
            else
                lst.push(html);
        }
        else {
            if (lst.indexOf(html) > 0) {

            }
            else

                lst.push(html);
            $('.multiSel').append(html);
            visited = 0;
            $(".hida").hide();
            
        }

    } else {

        selectedCount--;
        // debugger;
        var title1 = $(this)[0].id.substring(5, $(this)[0].id.length);
        
        // title1 = title1.replace(',', '');
        var html1 = '<span id="' + title1 + '">' + title1 + '</span>';

        if (lst.length > 0) {
            lstLength1 = lst.length;
            for (var count2 = 0; count2 < lst.length; count2++) {                
                if (lst[count2].indexOf(title1) > 0 || lst[count2].indexOf("," + title1) > 0) {                   
                    lst.splice(count2, 1);
                }
            }
        }
        if (selectedCount <= 4) {
            $('#more').remove();

            if (lst.length > 0) {

                for (var count1 = 0; count1 < lst.length; count1++) {
                    //    if (lst[count1].indexOf(title1) > 0) {
                    //        lst.splice(count1,1);
                    //       }
                    //   else {
                      if(visited ==1)
                    $('.multiSel').append(lst[count1]);
                }
                //  }


                lst = new Array();
            }
        }
        $('#' + title1).remove();
        var ret = $(".hida");
        $('.dropdown dt a').append(ret);

    }
});