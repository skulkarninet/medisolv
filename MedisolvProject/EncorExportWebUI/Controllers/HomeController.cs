﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.ComponentModel.Composition;
using EncorExportWebUI.ViewModels;
using EncorExportWebUI.Interfaces;
using EncorExportWebUI.Models;
using System.IO;
using System.IO.Compression;
using System.Web.UI.WebControls;
using VSMMefMvc.Interfaces;
using VSMMefMvc.Models;
using VSMMefMvc.ViewModels;
using PagedList;
using PagedList.Mvc;

namespace VSMMefMvc.Controllers
{
    public class HomeController : Controller
    {

        /// <summary>
        /// Gets the MeasureSets from Export plugins with interface 'IExportService' and 'IValidateMetaData'.
        /// </summary>
        /// <value>The validators.</value>
        [ImportMany]
        public IEnumerable<Lazy<IExportService, IValidateMetaData>> Validators { get; private set; }

        /// <summary>
        /// Gets the patient detail list for Grid population with interface 'IPatientDataService'.
        /// </summary>
        /// <value>The patient validator.</value>
        [Import]
        public Lazy<IPatientDataService> patientValidator { get; private set; }

        /// <summary>
        /// Gets the hospital validator.
        /// </summary>
        /// <value>The hospital validator.</value>
        [Import]
        public Lazy<IHospitalID> hospitalValidator { get; private set; }

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        public ActionResult Index()
        {
            var exportmodel = new ExportXmlModel();
            exportmodel.PatientList = new List<PatientModel>();
            var hospitalInfo = hospitalValidator.Value;
            List<string> hospitalIDs = new List<string>();
            hospitalIDs.AddRange(hospitalInfo.getHospitalID());
            exportmodel.HospitalIDs = new List<string>();
            //foreach (var hospital in hospitalIDs)
            //{
            //    exportmodel.HospitalIDs.Add(hospital);
            //}
            exportmodel.HospitalIDs.AddRange(hospitalIDs);
            // Following filter is used to load the available exports for the selected speciality for eg : OPPS export.
            exportmodel.Rules = new List<SelectListItem>(from v in Validators
                                                where v.Metadata.Measure.Contains("OPPS")
                                                || v.Metadata.Measure.Contains("IPPS")    
                          //the following split is used to extract the measure set name from the speciality                      
                       select new SelectListItem() {Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure});

            exportmodel.MeasureSetLst = new List<MeasureSets>();
            
            foreach (var measure in exportmodel.Rules)
            {
                exportmodel.MeasureSetLst.Add(new MeasureSets { Code = measure.Text, Name = measure.Value });
            }           

            return View(exportmodel);
        }

        /// <summary>
        /// Checks the Specialities
        /// </summary>
        /// <param name="selectedSpeciality">The selected speciality.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult CheckSpeciality(string selectedSpeciality)
        {
            //Object creation of ExportXmlModel  
            var vm = new ExportXmlModel();
            vm.PatientList = new List<PatientModel>();
            vm.HospitalIDs = new List<string>();
            //Following filter is used to load the available exports for the selected speciality for eg : OPPS export.
            if (selectedSpeciality == "OPPS")
            {
                vm.Rules = new List<SelectListItem>(from v in Validators
                                                    where v.Metadata.Measure.Contains("OPPS")
                                                     
                                                    //the following split is used to extract the measure set name from the speciality                      
                                                    select new SelectListItem() { Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure });
            }
            else if (selectedSpeciality=="IPPS")
            {
            vm.Rules = new List<SelectListItem>(from v in Validators
                                                where v.Metadata.Measure.Contains("IPPS")
                                               
                                                //the following split is used to extract the measure set name from the speciality                      
                                                select new SelectListItem() { Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure });
            }
            else
            {
                vm.Rules = new List<SelectListItem>(from v in Validators
                                                    where v.Metadata.Measure.Contains("OPPS")
                                                    || v.Metadata.Measure.Contains("IPPS")
                                                    //the following split is used to extract the measure set name from the speciality                      

                                                    select new SelectListItem() { Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure });            
            }
            var hospitalInfo = hospitalValidator.Value;
            List<string> hospitalIDs = new List<string>();
            hospitalIDs.AddRange(hospitalInfo.getHospitalID());
            vm.HospitalIDs = new List<string>();
            vm.HospitalIDs.AddRange(hospitalIDs);
            vm.MeasureSetLst = new List<MeasureSets>();
            foreach (var measure in vm.Rules)
            {
                vm.MeasureSetLst.Add(new MeasureSets { Code = measure.Text, Name = measure.Value });
            }
            return View("Index", vm);
        }

        public ActionResult SelectCheckedItems(string IPPKey,string test)
        {

            var vm = TempData["pd"] as ExportXmlModel;
            if (vm.DisplayForPatientList.Where(m =>m.IPPKey == IPPKey).FirstOrDefault().Selected)
            {
                for (int i = 0; i < vm.DisplayForPatientList.Count; i++)
                {
                    if (vm.DisplayForPatientList[i].Export == test && vm.DisplayForPatientList[i].IPPKey == IPPKey)
                    {
                        vm.DisplayForPatientList[i].Selected = false;
                    }
                }
                for (int i = 0; i < vm.PatientList.Count; i++)
                {
                    if (vm.PatientList[i].Export == test && vm.PatientList[i].IPPKey == IPPKey)
                    {
                        vm.PatientList[i].Selected = false;
                    }
                }
                //vm.DisplayForPatientList.Where(m => m.IPPKey == IPPKey).FirstOrDefault().Selected = false;
                //vm.PatientList.Where(m => (m.IPPKey == IPPKey) && (m.Export == test)).FirstOrDefault().Selected = false;
                // vm.PatientList.Where(m => m.IPPKey == IPPKey).FirstOrDefault().Selected = false;
            }
            else
            {
               // vm.DisplayForPatientList.Where(m => m.IPPKey == IPPKey).FirstOrDefault().Selected = true;
                for (int i = 0; i < vm.DisplayForPatientList.Count; i++)
                {
                    if (vm.DisplayForPatientList[i].Export == test && vm.DisplayForPatientList[i].IPPKey == IPPKey)
                    {
                        vm.DisplayForPatientList[i].Selected = true;
                    }
                }
                for (int i = 0; i < vm.PatientList.Count; i++)
                {
                    if (vm.PatientList[i].Export == test && vm.PatientList[i].IPPKey==IPPKey)
                    {
                        vm.PatientList[i].Selected = true;
                    }
                }
                //if (vm.PatientList.Where(m => m.Export == test).FirstOrDefault().Export == test)
                //{
                //    vm.PatientList.Where(m => m.IPPKey == IPPKey).FirstOrDefault().Selected = true;
                //}
                //vm.PatientList.Where(m => m.IPPKey == IPPKey).FirstOrDefault().Selected = true;
            }

            TempData["pd"] = vm;
            return View("Index", vm);
        }
               
        public ActionResult SelectCheckedAll(bool check)
        {
            var vm = TempData["pd"] as ExportXmlModel;
            for (int i=0;i<vm.DisplayForPatientList.Count;i++)
            {
                vm.DisplayForPatientList[i].Selected= check;
                for (int j = 0; j < vm.PatientList.Count; j++)
                {
                    if (vm.PatientList[j].Export == vm.DisplayForPatientList[i].Export && vm.PatientList[j].IPPKey == vm.DisplayForPatientList[i].IPPKey)
                    {
                        vm.PatientList[j].Selected = check;
                    }
                }
                //vm.PatientList.Where(m => (m.IPPKey == vm.DisplayForPatientList[i].IPPKey) && (m.Export == vm.DisplayForPatientList[i].Export)).FirstOrDefault().Selected = check;
                // vm.PatientList.Where(m => m.IPPKey == vm.DisplayForPatientList[i].IPPKey).FirstOrDefault().Selected = true;
            }
            
            TempData["pd"] = vm;
            return View("Index", vm);
        }
        /// <summary>
        /// Saves the form.
        /// </summary>
        /// <param name="vm">The vm.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult RefreshGrid(int? start,ExportXmlModel vm)
        {            
            ExportParameters parameters=new ExportParameters();
            //Object creation of ExportXmlModel for accessing UI Properties.
            var exportmodel = new ExportXmlModel();
           
            //Patient list to save 'Account Number','Name','MeasureSet','Unit Number'.
            exportmodel.PatientList = new List<PatientModel>();
            
            var patientInfo = patientValidator.Value;

            //Get selected MeasureSets
            var measures =vm.MeasureSetLst.Where(m => m.IsSelected).Select(m => m.Code).ToList();
            vm.counter = vm.MeasureSetLst.Where(m => m.IsSelected == true).Count();
            var hospitalInfo = hospitalValidator.Value;
            List<string> hospitalIDs = new List<string>();
            vm.HospitalIDs=new List<string>();
            //Get selected MeasureSets
                       
            vm.HospitalIDs = new List<string>();
            foreach (var measure in measures)
            {
                hospitalIDs.AddRange(hospitalInfo.getHospitalID());
            }
            vm.HospitalIDs = hospitalIDs.Distinct().ToList();

            //Gets patient details from Hashtable.
            parameters = patientInfo.GetPatientInfo(measures, vm.period.StartDate, vm.period.EndDate,vm.SelectedSpeciality,vm.SelectedHospID);                            
          
            vm.PatientList = new List<PatientModel>();            
            vm.PatientList = parameters.PatientList;
            TempData["pd"] = vm;
            vm.PatientInfoList = vm.PatientList.ToPagedList(start ?? 1, 10);
            vm.DisplayForPatientList = vm.PatientInfoList.ToList();

            return View("Index", vm);
        }

        [HttpPost]
        public ActionResult RefreshHospitalID(ExportXmlModel vm)
        {
            //Object creation of ExportXmlModel for accessing UI Properties.
            var exportmodel = new ExportXmlModel();

            var hospitalInfo = hospitalValidator.Value;
            List<string> hospitalIDs = new List<string>();
            //Get selected MeasureSets
            var measures = vm.MeasureSetLst.Where(m => m.IsSelected).Select(m => m.Code).ToList();
            vm.HospitalIDs = new List<string>();
            vm.counter = vm.MeasureSetLst.Where(m => m.IsSelected == true).Count();
            foreach (var measure in measures)
            {
                hospitalIDs.AddRange(hospitalInfo.getHospitalID());
            }
            vm.HospitalIDs = hospitalIDs.Distinct().ToList();
            return View("Index", vm);
        }

        public ActionResult PaginateGrid(int? start)
        {
            ExportParameters parameters = new ExportParameters();
            ExportXmlModel vm = (ExportXmlModel)TempData["pd"];
           

            //Object creation of ExportXmlModel for accessing UI Properties.
            var exportmodel = new ExportXmlModel();

            //Patient list to save 'Account Number','Name','MeasureSet','Unit Number'.
            exportmodel.PatientList = new List<PatientModel>();

            var patientInfo = patientValidator.Value;
            vm.counter = vm.MeasureSetLst.Where(m => m.IsSelected == true).Count();
            //Get selected MeasureSets
            var measures = vm.MeasureSetLst.Where(m => m.IsSelected).Select(m => m.Code).ToList();

            var hospitalInfo = hospitalValidator.Value;
            List<string> hospitalIDs = new List<string>();
            vm.HospitalIDs = new List<string>();

            foreach (var measure in measures)
            {
                hospitalIDs = hospitalInfo.getHospitalID();
            }
            vm.HospitalIDs = hospitalIDs;              

            vm.PatientInfoList = vm.PatientList.ToPagedList(start ?? 1, 10);
            vm.DisplayForPatientList = vm.PatientInfoList.ToList();
            TempData["pd"] = vm;
            return View("Index", vm);
        }

        /// <summary>
        /// Indexes the specified vm.
        /// </summary>
        /// <param name="vm">The vm.</param>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        public ActionResult Export(int? start,ExportXmlModel vm)
        {
            int zipCounter = 0;
            //var temp = TempData["pd"];
            vm = (ExportXmlModel)TempData["pd"];
            var hospitalInfo = hospitalValidator.Value;       
            List<bool> selectedPatient = new List<bool>();

            //Gets selected Patients
            foreach (var patient in vm.DisplayForPatientList)
            {
                selectedPatient.Add(patient.Selected);
            }

            int selectedMeasuresetCount = vm.MeasureSetLst.Count(item => item.IsSelected);        
                
            ExportParameters parameters = new ExportParameters();
            var exportmodel = new ExportXmlModel();
            exportmodel.PatientList = new List<PatientModel>();

            List<string> hospitalIDs = new List<string>();
            vm.HospitalIDs = new List<string>();
            //Get selected MeasureSets       
            var measures = vm.MeasureSetLst.Where(m => m.IsSelected).Select(m => m.Code).ToList();
            vm.HospitalIDs = new List<string>();
            foreach (var measure in measures)
            {
                hospitalIDs.AddRange(hospitalInfo.getHospitalID());
            }
            vm.HospitalIDs = hospitalIDs.Distinct().ToList();

            var patientInfo = patientValidator.Value;

            //Gets patient details.
            parameters = patientInfo.GetPatientInfo(measures, vm.period.StartDate, vm.period.EndDate, vm.SelectedSpeciality, vm.SelectedHospID);

            for (int i = 0; i < vm.PatientList.Count; i++)
            {
                parameters.PatientList[i].Selected = vm.PatientList[i].Selected;
            }

            vm.PatientList = new List<PatientModel>();
            vm.PatientList = parameters.PatientList;
            parameters.PatientList = vm.PatientList;
            parameters.gid = Guid.NewGuid().ToString();

            //Set Period from UI to ExportParameters "SelectedPeriod"
            parameters.SelectedPeriod = new Period { EndDate = vm.period.EndDate, StartDate = vm.period.StartDate };

            //Check selected Submission ActionCode from UI
            parameters.SelectedAction = (vm.SelectedAction == "ADD" ? "ADD" : "DELETE");

            //Set Selected hospitalID from UI to ExportParameters "SelectedHospID"
            parameters.SelectedHospID = vm.SelectedHospID;                   

            //This loop calls ExportXML method for selected Measureset
            foreach(var item in vm.MeasureSetLst)
            { 
                if (item.IsSelected)
                {
                    var rule = (from v in Validators
                                where v.Metadata.Measure == item.Name
                                select v.Value).FirstOrDefault();                   

                    zipCounter++;
                    //Checks whether last Export to generate Zip
                    if (measures.Count>1)
                    {
                        parameters.IsMultipleSelect = true;
                    }
                    if (zipCounter == selectedMeasuresetCount && zipCounter > 1)
                    {
                        parameters.isLastPlugin = true;
                    }
                    var result = rule.ExportXML(parameters);
                    if (!result.IsValid)
                    {
                        vm.StatusLabel = "Error Occured While Exporting!";
                        ModelState.AddModelError("Input", result.ErrorMessage);
                    }
                    else
                    {                        
                        vm.StatusLabel = "Export created Successfully on Server at location: 'D:\u005cXMLOutput'!";
                    }
                }               
            }
            vm.counter = vm.MeasureSetLst.Where(m => m.IsSelected == true).Count();
            if (vm.SelectedSpeciality.Equals("IPPS"))
            {
                vm.Rules = new List<SelectListItem>(from v in Validators
                                                    where v.Metadata.Measure.Contains("IPPS")
                                                    //the following split is used to extract the measure set name from the speciality                      
                                                    select new SelectListItem() { Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure });
            }
            else if (vm.SelectedSpeciality.Equals("OPPS"))
            {
                vm.Rules = new List<SelectListItem>(from v in Validators
                                                    where v.Metadata.Measure.Contains("OPPS")
                                                    //the following split is used to extract the measure set name from the speciality                      
                                                    select new SelectListItem() { Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure });
            }
            else
            {
                vm.Rules = new List<SelectListItem>(from v in Validators
                                                    where v.Metadata.Measure.Contains("OPPS")
                                                    || v.Metadata.Measure.Contains("IPPS")
                                                    //the following split is used to extract the measure set name from the speciality                      
                                                    select new SelectListItem() { Text = v.Metadata.Measure.Split('-')[1], Value = v.Metadata.Measure });
            }
            TempData["pd"] = vm;            
            return View("Index",vm);
        }
        

    }
}
