﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSMMefMvc.Interfaces
{
    /// <summary>
    /// Interface IHospitalID
    /// </summary>
    public interface IHospitalID
    {
        /// <summary>
        /// Gets the hospital identifier.
        /// </summary>
        /// <param name="measureSet">The measure set.</param>
        /// <returns>List&lt;System.String&gt;.</returns>
        List<string> getHospitalID();
    }
}
