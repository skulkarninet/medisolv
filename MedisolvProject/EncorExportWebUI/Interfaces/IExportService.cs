﻿using EncorExportWebUI.Models;

namespace EncorExportWebUI.Interfaces
{
    /// <summary>
    /// Interface IExportService
    /// </summary>
    public interface IExportService
    {
        /// <summary>
        /// Exports the XML.
        /// </summary>
        /// <param name="exportParamters">The export paramters.</param>
        /// <returns>ExportResult.</returns>
        ExportResult ExportXML(ExportParameters exportParamters);

        /// <summary>
        /// Exports the CSV.
        /// </summary>
        /// <param name="exportParameters">The export parameters.</param>
        /// <returns>ExportResult.</returns>
        ExportResult ExportCSV(ExportParameters exportParameters);
    }
}