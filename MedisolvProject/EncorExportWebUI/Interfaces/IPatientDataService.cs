﻿using System;
using System.Collections;
using System.Collections.Generic;
using EncorExportWebUI.Models;

namespace VSMMefMvc.Interfaces
{
    /// <summary>
    /// Interface IPatientDataService
    /// </summary>
    public interface IPatientDataService
    {
        /// <summary>
        /// Gets the patient information.
        /// </summary>
        /// <param name="measure">The measure.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="selected">The selected.</param>
        /// <returns>Hashtable.</returns>
        ExportParameters GetPatientInfo(List<string> measure, DateTime startDate, DateTime endDate, string selected,string selectedHospitalID);
    }
}
