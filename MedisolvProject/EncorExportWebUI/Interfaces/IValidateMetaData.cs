﻿
namespace EncorExportWebUI.Interfaces
{
    /// <summary>
    /// Interface IValidateMetaData
    /// </summary>
    public interface IValidateMetaData
    {
        /// <summary>
        /// Gets the measure.
        /// </summary>
        /// <value>The measure.</value>
        string Measure { get; }
    }
}