﻿
namespace VSMMefMvc.Models
{
    /// <summary>
    /// Class PatientModel.
    /// </summary>
    public class PatientModel
    {
        /// <summary>
        /// Gets or sets the patient numbers.
        /// </summary>
        /// <value>The patient numbers.</value>
        public string patientNumbers { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PatientModel"/> is selected.
        /// </summary>
        /// <value><c>true</c> if selected; otherwise, <c>false</c>.</value>
        public bool Selected { get; set; }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        /// <value>The account number.</value>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the name of the patient.
        /// </summary>
        /// <value>The name of the patient.</value>
        public string patientName { get; set; }

        /// <summary>
        /// Gets or sets the measure set.
        /// </summary>
        /// <value>The measure set.</value>
        public string MeasureSet { get; set; }

        public string IPPKey { get; set; }

        public string Export { get; set; }

        public string HospitalID { get; set; }

    }
}