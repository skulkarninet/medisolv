﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EncorExportWebUI.Models
{
    /// <summary>
    /// Class Period.
    /// </summary>
    public class Period
    {
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; set; }
    }
}