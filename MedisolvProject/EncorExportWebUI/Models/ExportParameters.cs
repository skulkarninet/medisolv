﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VSMMefMvc.Models;

namespace EncorExportWebUI.Models
{
    /// <summary>
    /// Class ExportParameters.
    /// </summary>
    public class ExportParameters
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExportParameters"/> class.
        /// </summary>
        public ExportParameters()
        {
            SelectedPeriod = new Period();
        }

        /// <summary>
        /// Gets or sets the selected measures.
        /// </summary>
        /// <value>The selected measures.</value>
        public string[] SelectedMeasures { get; set; }
        /// <summary>
        /// Gets or sets the selected period.
        /// </summary>
        /// <value>The selected period.</value>
        public Period SelectedPeriod { get; set; }
        /// <summary>
        /// Gets or sets the output folder.
        /// </summary>
        /// <value>The output folder.</value>
        public string OutputFolder { get; set; }
        /// <summary>
        /// Gets or sets the selected action.
        /// </summary>
        /// <value>The selected action.</value>
        public string SelectedAction { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is multiple select.
        /// </summary>
        /// <value><c>true</c> if this instance is multiple select; otherwise, <c>false</c>.</value>
        public bool IsMultipleSelect { get; set; }
        /// <summary>
        /// Gets or sets the selected hosp identifier.
        /// </summary>
        /// <value>The selected hosp identifier.</value>
        public string SelectedHospID { get; set; }
        /// <summary>
        /// Gets or sets the gid.
        /// </summary>
        /// <value>The gid.</value>
        public string gid { get; set; }
        /// <summary>
        /// Gets or sets the patient list.
        /// </summary>
        /// <value>The patient list.</value>
        public List<PatientModel> PatientList { get; set; }

        public bool isLastPlugin { get; set; }
    }
}