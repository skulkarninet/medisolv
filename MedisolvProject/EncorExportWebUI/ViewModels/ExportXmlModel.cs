﻿using System.Collections.Generic;
using System.Web.Mvc;
using VSMMefMvc.Models;
using VSMMefMvc.ViewModels;

namespace EncorExportWebUI.ViewModels
{
    /// <summary>
    /// Class ExportXmlModel.
    /// </summary>
    public class ExportXmlModel
    {
        /// <summary>
        /// Gets or sets the input.
        /// </summary>
        /// <value>The input.</value>
        public string Input { get; set; }
        /// <summary>
        /// Gets or sets the measure set.
        /// </summary>
        /// <value>The measure set.</value>
        public List<SelectListItem> MeasureSet { get; set; }
        /// <summary>
        /// Gets or sets the rules.
        /// </summary>
        /// <value>The rules.</value>
        public List<SelectListItem> Rules { get; set; }
        /// <summary>
        /// Gets or sets the status label.
        /// </summary>
        /// <value>The status label.</value>
        public string StatusLabel { get; set; }
        /// <summary>
        /// Gets or sets the selected measure set.
        /// </summary>
        /// <value>The selected measure set.</value>
        public string selectedMeasureSet { get; set; }
        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        /// <value>The period.</value>
        public EncorExportWebUI.Models.Period period { get; set; }
        /// <summary>
        /// Gets or sets the speciality.
        /// </summary>
        /// <value>The speciality.</value>
        public List<SelectListItem> Speciality { get; set; }
        /// <summary>
        /// Gets or sets the selected speciality.
        /// </summary>
        /// <value>The selected speciality.</value>
        public string SelectedSpeciality { get; set; }
        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        /// <value>The action.</value>
        public List<SelectListItem> Action { get; set; }
        /// <summary>
        /// Gets or sets the selected action.
        /// </summary>
        /// <value>The selected action.</value>
        public string SelectedAction { get; set; }
        /// <summary>
        /// Gets or sets the measure set LST.
        /// </summary>
        /// <value>The measure set LST.</value>
        public List<MeasureSets> MeasureSetLst { get; set; }       
        /// <summary>
        /// Gets or sets the selected hosp identifier.
        /// </summary>
        /// <value>The selected hosp identifier.</value>
        public string SelectedHospID { get; set; }
        /// <summary>
        /// Gets or sets the patient list.
        /// </summary>
        /// <value>The patient list.</value>
        public List<PatientModel> PatientList { get; set; }

        public List<string> HospitalIDs { get; set; }
        public PagedList.IPagedList<PatientModel> PatientInfoList { get; set; }

        public List<PatientModel> DisplayForPatientList { get; set; }

        public List<string> MyList { get; set; }

        public int counter { get; set; }

    }
}